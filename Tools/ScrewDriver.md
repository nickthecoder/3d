A screwdriver for the common size screwdriver bits.

The body can store spare bits, and the end can free-wheel.

There are two versions of the body depending on `useCollet` parameter.
Instead of using a collet, the end is a fixed hexagonal hole,
into which you should glue a magnet (use 2 part epoxy).

The bits are held in place using a "collet".
The nut is tapered, so that it starts off easy going, and gets progressively tighter
as you turn it.
If the bits are too loose, then increase `snug`. But if the collet tightens too soon,
then decrease it. (Alternatively, you could scale the nut in X & Y before slicing).

I printed these in ABS, but other plastics should also work fine.
However, as PLA is brittle, if you tighten the collet without a bit in place,
then it would probably break.

The collet only has to be tightened enough to stop the bit from falling out.
Don't overtighten.

