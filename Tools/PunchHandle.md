I have a punch to make holes in fabric for eyelets.
Rather than hit it with a hammer, I wonder if I can use it
like a drill bit.
The 12mm punch is too big for my cordless drill, so maybe a
simple handle is good enough???

The side has a hole for an M6 threaded insert,
for a bolt to work like a grub screw, holding the punch in place.

Melt the threaded insert while the punch is in-place.
Otherwise, the melted plastic may stray into the hole for the punch.
