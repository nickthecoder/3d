Zip ties inspired by SunShine :
https://www.youtube.com/watch?v=AJyiSVeIEqY

Thread some TPU through the holes to complete the zip tie.

NOTE. Do NOT try to print one at a time, as the small size will probably
prevent one layer cooling before the next layer starts.
