## Screw Size Guage

For use with FooCAD's "Screws" extension.

Helps you choose the holeDiameter and headDiameter paramaters of
Countersink / KeyholeHanger for the screws you plan to use.

