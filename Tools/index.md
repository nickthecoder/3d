# Tools
## [AdhesiveSpreader](AdhesiveSpreader.foocad)

[![AdhesiveSpreader](AdhesiveSpreader.png)](AdhesiveSpreader.foocad)

## [AllenKeyHolder](AllenKeyHolder.foocad)

[![AllenKeyHolder](AllenKeyHolder.png)](AllenKeyHolder.foocad)

## [BandClampCorner](BandClampCorner.foocad)

[![BandClampCorner](BandClampCorner.png)](BandClampCorner.foocad)

## [CableTieMount](CableTieMount.foocad)

[![CableTieMount](CableTieMount.png)](CableTieMount.foocad)

## [CenterGuage](CenterGuage.foocad)

[![CenterGuage](CenterGuage.png)](CenterGuage.foocad)

## [CircularSawFrame](CircularSawFrame.foocad)

[![CircularSawFrame](CircularSawFrame.png)](CircularSawFrame.foocad)

## [Clamp](Clamp.foocad)

[![Clamp](Clamp.png)](Clamp.foocad)

## [ClampEndStop](ClampEndStop.foocad)

[![ClampEndStop](ClampEndStop.png)](ClampEndStop.foocad)

## [CornerBracket](CornerBracket.foocad)

[![CornerBracket](CornerBracket.png)](CornerBracket.foocad)

## [CornerClamp](CornerClamp.foocad)

[![CornerClamp](CornerClamp.png)](CornerClamp.foocad)

## [DeburringTool](DeburringTool.foocad)

[![DeburringTool](DeburringTool.png)](DeburringTool.foocad)

## [DrawerSlide](DrawerSlide.foocad)

[![DrawerSlide](DrawerSlide.png)](DrawerSlide.foocad)

## [DrillBitProtector](DrillBitProtector.foocad)

[![DrillBitProtector](DrillBitProtector.png)](DrillBitProtector.foocad)

## [DriveWheel](DriveWheel.foocad)

[![DriveWheel](DriveWheel.png)](DriveWheel.foocad)

## [Funnel](Funnel.foocad)

[![Funnel](Funnel.png)](Funnel.foocad)

## [GuideHoleBlock](GuideHoleBlock.foocad)

[![GuideHoleBlock](GuideHoleBlock.png)](GuideHoleBlock.foocad)

## [JunctionBox](JunctionBox.foocad)

[![JunctionBox](JunctionBox.png)](JunctionBox.foocad)

## [LensCap](LensCap.foocad)

[![LensCap](LensCap.png)](LensCap.foocad)

## [NutBoltAndInsert](NutBoltAndInsert.foocad)

[![NutBoltAndInsert](NutBoltAndInsert.png)](NutBoltAndInsert.foocad)

## [PunchHandle](PunchHandle.foocad)

[![PunchHandle](PunchHandle.png)](PunchHandle.foocad)

## [RazorBladeKnife](RazorBladeKnife.foocad)

[![RazorBladeKnife](RazorBladeKnife.png)](RazorBladeKnife.foocad)

## [RoundBoltHead](RoundBoltHead.foocad)

[![RoundBoltHead](RoundBoltHead.png)](RoundBoltHead.foocad)

## [RouterRadius](RouterRadius.foocad)

[![RouterRadius](RouterRadius.png)](RouterRadius.foocad)

## [ScrewDriver](ScrewDriver.foocad)

[![ScrewDriver](ScrewDriver.png)](ScrewDriver.foocad)

## [ScrewSizes](ScrewSizes.foocad)

[![ScrewSizes](ScrewSizes.png)](ScrewSizes.foocad)

## [SharpeningStoneBox](SharpeningStoneBox.foocad)

[![SharpeningStoneBox](SharpeningStoneBox.png)](SharpeningStoneBox.foocad)

## [SliderMount](SliderMount.foocad)

[![SliderMount](SliderMount.png)](SliderMount.foocad)

## [SlidingKnife](SlidingKnife.foocad)

[![SlidingKnife](SlidingKnife.png)](SlidingKnife.foocad)

## [StrapWratchet](StrapWratchet.foocad)

[![StrapWratchet](StrapWratchet.png)](StrapWratchet.foocad)

## [StubbyScrewDriver](StubbyScrewDriver.foocad)

[![StubbyScrewDriver](StubbyScrewDriver.png)](StubbyScrewDriver.foocad)

## [TapAndDieInsert](TapAndDieInsert.foocad)

[![TapAndDieInsert](TapAndDieInsert.png)](TapAndDieInsert.foocad)

## [TapeMeasureClip](TapeMeasureClip.foocad)

[![TapeMeasureClip](TapeMeasureClip.png)](TapeMeasureClip.foocad)

## [ThickWalledBox](ThickWalledBox.foocad)

[![ThickWalledBox](ThickWalledBox.png)](ThickWalledBox.foocad)

## [Toothpick](Toothpick.foocad)

[![Toothpick](Toothpick.png)](Toothpick.foocad)

## [TrolleyKey](TrolleyKey.foocad)

[![TrolleyKey](TrolleyKey.png)](TrolleyKey.foocad)

## [ZipTie](ZipTie.foocad)

[![ZipTie](ZipTie.png)](ZipTie.foocad)

