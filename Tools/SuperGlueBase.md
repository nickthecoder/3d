My favourite brush-on super glue has a very narrow bottle which is easily knocked over.

It is designed to be a snug fit, so that lifting the bottle also lifts the base.

Print using TPU (or other flexible filament).
You could use a rigid filament, but the fit would need to be perfect,
or use a shim to jam the bottle into this base.
