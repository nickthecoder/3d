Use @Piece [diameterVariations] to calibrate the [diameter] of the holes : .
The TPU should fit in the holes easily, but with as little clearance as possible.
i.e. you need to push to get it though.

Next, use @Piece [overhangVariations] to calibrate the values of [overhang].
Too large, and the TPU won't fit through the holes easily.
Too small, and the TPU will slip out too easily.
