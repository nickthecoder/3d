A mini clamp, useful for adding pressure when glueing small items.

The pieces are easy to print. No supports needed.

However, I recomment a brim when printing the long thin bolt,
to ensure it does get knocked over.

It produces a reasonable pressure, but I'm sure it's very easy to
strip the threads if you tighten a lot.

If the threads do strip, you could melt in a threaded insert,
and use a regular metal bolt ;-))

