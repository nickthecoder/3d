# Electronics
## [CoinCellLED](CoinCellLED.foocad)

[![CoinCellLED](CoinCellLED.png)](CoinCellLED.foocad)

## [Grommet](Grommet.foocad)

[![Grommet](Grommet.png)](Grommet.foocad)

## [PSUHack](PSUHack.foocad)

[![PSUHack](PSUHack.png)](PSUHack.foocad)

## [PowerSupply](PowerSupply.foocad)

[![PowerSupply](PowerSupply.png)](PowerSupply.foocad)

## [SevenSegmentDisplay](SevenSegmentDisplay.foocad)

[![SevenSegmentDisplay](SevenSegmentDisplay.png)](SevenSegmentDisplay.foocad)

## [USBBackPlate](USBBackPlate.foocad)

[![USBBackPlate](USBBackPlate.png)](USBBackPlate.foocad)

