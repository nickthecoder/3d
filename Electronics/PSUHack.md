I bought 2nd hand HP server power supply, just to use it as a 12V supply.
So I want a small housing on one end to cover the connector.

These PSUs are cheap (2nd hand), very efficient, and very powerful.
My first one will be for the 12V lighting in the garden (the PSU is inside!)

FYI, To turn the PSU on, connect pins 33 and 36
(Some PSUs require a resistor, others a short is fine. YMMV).
