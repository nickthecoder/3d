Holds an LED to a coin cell, for Xmas decorations etc.
Should be easy to insert/remove the coin cell without the LED falling out.

The will be out of sight (e.g. inside my Springo Snowman), so they can look naff!

Note, the LED's leads are not shown on the preview.
They go in and out around the castellations.

You can print small versions, but the default side allows it to be
stood up on its end.
