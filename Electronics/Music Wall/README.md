Build it in sections. Each section has a grid of CDs, and an arduino
reading the buttons and controlling the LEDs.

In addition, there is a "master" (Maybe a Raspberry PI) connected to each
arduino.

Could connect using USB (using Serial UART).
A Model3 PI has 4 USB sockets, so 4 sections would be easy.

However, if we use I2C, then each section can be chained to the next, and
there we can add as many sections as we like (well, at least 100).




For the "Keyboard"
==================

I could use a dedecated keyscan IC, or...

shift registers to address the rows, and multiplexers to scan "manually".


For the LEDs
============

We don't need to address LEDs individually, as we only need four corners of a CD on at any time.
There are 9 rows and 17 columns.
Turn any number of columns HIGH (will always be two), using 2 shift registers.
Turn any number of rows LOW (will always be two), using 3 shift registers.

2 shared pins for latch and data
2 pins for the two clocks

Total: 2 + 2 shared

Split the job into 2 boards. Each has 8x8 CDs.
==============================================

One board will be bigger than the other, as it will have 8.5 CDs wide, and the other
7.5 CDs wide.

Must ensure that board boards control 8 columns, so the board 7.5 CDs wide has the buttons on
its edge, but the other board doesn't.
One board will have an extra shift register for the last column of LEDs.

Connections between boards
--------------------------

9 lines for the LED rows
8 lines for the button rows

GND (RED)
Vcc (BLACK)

3 button mux address (WHITE)
2 button mux data    (GREEN)

2 for LED column shift register data and latch (shared by all shift registers)
 (data = ORANGE, LATCH=YELLOW)

1 for LED column clock (GREY)


Each connection should be quite short, and connects to header pins on a pcb near the edge of
the board. Female right angled headers on the pcbs and straight connectors with male pins
at both ends. Use cable tidy stuff to bind the bundles of wires together.

Use a 5 and 4 connector glued back to back for LED columns.
    Can't get that the wrong way round!
A 5 and a 3 glued back to back for button rows
    Make it near mirror image, so that buttons and leds can't get mixed up.

5 way connector for the mux address and data,
5 way connector for GND, VCC, led column clock, latch and data
==
27
==

OR use RJ45 connectors
8 for button rows
8 for led rows (with one row missing)

6 for missed led row, latch, column clock, data (4) GND, VCC (2)
5 for mux address and data (3 + 2)
==
27
==

Maybe use BROWN for the Y direction of buttons/leds.
and BLUE for the X direction of buttons/leds.
Oooh - use BROWN for buttons in Y direction and BLUE in X direction
Reverse the colours for the leds, so that the interconnects cannot be mistaken.
Make sure that BROWN ends up on PLUS LED terminal.
    Currently, the ROWS are LOW for ON, the COLUMNS are HIGH for ON, therefore, 
    BROWN needs to run VERTICALLY for LEDs.


Each button connects its row wire with the column wire.
Row wires are being set to HIGH or LOW depending on the scan line.
Each COLUMN is pulled down to ground, so normally it reads as LOW.
When button is pressed, the column connects directly to a ROW, and therefore becomes HIGH (when
that row is being scanned).




Or Split the job into 4 boards
==============================

Each has 4x8 CDs. Board sizes 4.5, 3.5, 4.5, 3.5



Connections between boards
--------------------------

Same as the other case, but also between 1&2 and 3&4 :
    4 lines to select the keyboard columns
    4 lines to select the LED columns


Wiring
------

Could use RJ45 connections to interconnect the boards.
One for LED rows (8lines)
One for Mux address + data (5 lines)
One for the remainder (5lines)

One for half board interconnects (8lines)

All shift registers can use the same latch and data pins (use the SPI pins), and each
group has its own clock pins.

To keep the profile small, the circuit boards should be against the wall, with
a hole cut in the board to recess them.
Print out a surround which fits into the board's hole, and staples to it.
It has mouting holes for the board to screw into. (Tap the plastic holes).

CDs in rows 2 and 6 house all the stuff (so that the corners are free to screw to the wall).


TM1637 Display Driver chip
==========================

Instead of mucking about with lots of muxes, and shift registers, just use one type of chip.
TM1637 has upto 6 banks of 8 LEDs and 2 banks of 8 switches. Therefore one chip can handle 2 columns,
only using 3 banks of LEDs and both banks of switches.

Uses an I2C style comms (without the address part).
Use a home-made chip-enable for each, by forcing the clocks high for all but the active chip.
OR have a clock for each one, either directly from the Arduino (I think there are enough pins),
but via a mux otherwise.

For 16 columns, I'd need 8 chips, with the last chip using another bank of LEDs for the final column.
Perect, because the chip enable could be done via a shift register.

Can then make the boards any size - probably 4 columns each.

Could use one more chip to handle miscellaneous I/O such as "Stop/Skip/Back/Mode".
Has its own dedicated chip enable line.

Wiring
------

LED and Buttons rows do NOT need to connect from one board to another.
    HUGE win ;-)

8 clocks + 1 data line is all that's needed for 16 columns
Or 9 clocks + 1 data line if the last column of LEDs are handled by a separate chip.

If the FIRST chip is the special one, then we only need 8 lines between boards 0 and 1.
7 lines between boards 1 and 2 etc.
Each board uses the 0th pin for its clock, and passes pins 1..7 to pins 0..6 of the next board.
That way, the boards can be chained together in any order.

Could use RJ45, but would that be too thick?
Use Cat 5 cable, but have right angle header pins.
Just one PCB per panel, and the Cat 5 cable is wired directly onto the board at one end.


Circuits
========

Use a nano for ease, but mount it without the black plastic to make it slimline (remove ICSP header).
OR, use right angled header pins pointing in the SAME direction.



