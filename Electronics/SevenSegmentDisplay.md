A home-made 7 segment display.

Drill holes for LED pins (using as many LEDs as you want).
Note, you can solder the LEDs, with very minimal melting of plastic!

Cover with acetate sheets of the same colour as your LEDs.
        
Try to use LEDs with a large angle of light (not highly directional),
and consider bending the LEDs over so that they light up sidewards
rather the straight outwards.

## Print Notes
    
Either swap from white to black filament for the last couple of layers,
or just colour the front black with a permanent marker.

Do not use transparent or semi-transparent filament, or
light will leak from segment to the next.
