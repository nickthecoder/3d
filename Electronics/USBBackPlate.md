A simple housing for some double usb sockets, which are designed to be soldered onto a circuit
board. However, I don't need the circuit board, and want to attach them to the back of a box.

The sockets I used are cheap on ebay :
https://www.ebay.co.uk/itm/254378889580
Titled Dual USB 2.0 Type A Female Socket PCB Jack Connector SMT
(despite them not being SMT components!)

I only want them for power delivery, and I'll be soldering wires directly to the pins.

The part comes in two pieces, to be glued together after the sockets are installed

Solder the wires, then rotate each socket into the boxes, adding a dab of glue to keep them in place.
Avoid gluing the spingy parts!

Glue the "extra" piece to the top of the "main" piece.
