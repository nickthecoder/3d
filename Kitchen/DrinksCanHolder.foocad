import static uk.co.nickthecoder.foocad.layout.v1.Layout2d.*
import static uk.co.nickthecoder.foocad.layout.v1.Layout3d.*

interface Design {
    fun profile( height : double, diameter : double, thickness : double ) : Shape2d
}

class Faceted(

    val sides : int,
    val twist : double

) : Design {
    override fun profile( height : double, diameter : double, thickness : double ) : Shape2d {
        return (
            Circle( diameter/2 + thickness ).sides(sides) -
                Square( thickness+1, 0.1 ).centerY().leftTo( diameter/2 )
                    .repeatAround( sides )
            ).rotate( height*twist )
                
    }
}

class Bobbles(

    val sides : int

) : Design {
    override fun profile( height : double, diameter : double, thickness : double ) : Shape2d {
        return Circle( diameter / 2 - thickness ).sides(30 * sides) +
            Circle( thickness ).sides(30)
                .translateX( diameter / 2 )
                .hull( Circle( thickness ).sides(30) )
                .repeatAround( sides )
    }
}


class DrinksCanHolder : AbstractModel() {

    var size = Vector2( 60, 90 )
    var thickness = 5

    var bottomChamfer = Vector2( 6, 8 )
    var topChamfer = 4
    var steps = 8

    override fun build() : Shape3d {

        val design = Bobbles( 10 ) // Faceted(20, 0.5)

        return ExtrusionBuilder().apply {
            joinStrategy = OneToOneJoinStrategy()
            crossSection( design.profile( 0, size.x - bottomChamfer.x * 2, 0.1 ) )
            forward( bottomChamfer.y )
            val stepSize = (size.y - bottomChamfer.y - topChamfer) / steps
            for (step in 0 until steps) {
                val height = bottomChamfer.y + step * stepSize
                crossSection( design.profile( height, size.x, thickness ) )            
                forward( stepSize )
            }
            forward( topChamfer - stepSize )
            crossSection( design.profile( size.y, size.x - topChamfer * 2, 0.1 ) )
        }.build()
    }

}
