A simple lid for food and drink cans.
NOTE, this is NOT air tight, especially for the drink cans (pressure will NOT build up).
However, if I tip a can over, the liquid does not come out, but may well drip over time.

I have two use cases in mind :

1) To cover up a half full can of drink (especially useful when I'm doing DIY)
2) For my Mum - When feeding canned dog food to her small dog, she doesn't use a full can
   in one sitting. She bought plastic tin can covers, but these weren't a tight fit, and fall off!

Note, I don't expect the food to be in contact with the lid, so I don't think the plastic
needs to be food grade. (In general do NOT use 3D printed parts for food, because the layer
lines are a great place for food scraps to collect, and feed bacteria!)
