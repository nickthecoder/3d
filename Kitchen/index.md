# Kitchen
## [CanLid](CanLid.foocad)

[![CanLid](CanLid.png)](CanLid.foocad)

## [ColdCanContraption](ColdCanContraption.foocad)

[![ColdCanContraption](ColdCanContraption.png)](ColdCanContraption.foocad)

## [CutleryDrainer](CutleryDrainer.foocad)

[![CutleryDrainer](CutleryDrainer.png)](CutleryDrainer.foocad)

## [DrinksCanHolder](DrinksCanHolder.foocad)

[![DrinksCanHolder](DrinksCanHolder.png)](DrinksCanHolder.foocad)

## [TinOpenerHandle](TinOpenerHandle.foocad)

[![TinOpenerHandle](TinOpenerHandle.png)](TinOpenerHandle.foocad)

## [UnderShelf](UnderShelf.foocad)

[![UnderShelf](UnderShelf.png)](UnderShelf.foocad)

