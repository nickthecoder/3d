NOTE. I have NOT tested this yet!

A reimplementation of the cutlery drainer that I first created using OpenSCAD.
If I were to do it from scratch, I would probably design the profiles in Inkscape.
But I've chosen to replicate the old profiles exactly.

You can print it in one piece (abcd) two pieces (ab and cd), or as four individual pieces.
I originally printed it in two pieces because my printer at the time wasn't big enough
to print it in one piece. However, I've grown to like it in two pieces.

If you want colourful... Why not print them separately in different colours, and then
glue them together if you want them joined.

