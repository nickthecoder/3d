A slinky spring in the shape of a snowman.
The snowman is cut into a spring from his neck to the base. However,
I like to leave the first few loops fused, as it is nicer to play with
when the spring stops short of the base. I also leave the brim on!

You will need to separate the layers using using a blade.

##Print Notes

I used PETG for the spring(s).

I strongly recommend printing the "test" piece first, because you may need
to adjust the "gap", or your printer if your prints are fused.

I used a gap of 0.39mm, and a layer height of 0.2mm. If you use a different
layer height, also adjust the gap.

If you turn the part cooling fan off when printing the hat, the center
sags quite nicely!
