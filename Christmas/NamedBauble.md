NOTE. This SLOW to render in OpenSCAD.
MY first attempt used a single cylinder with LOTS of hole punched in it
but this was rediculously slow (I gave up waiting for it to render!)
This implementation is still slow, but not unbearable!

NOTE. using resolutionX(3) caused the whole model to render to nothing!
Not sure why, but I suspect it relates to total complexity of the model
exceeding a maximum value in OpenSCAD.
