A Springy Christmas Tree decoration.
The trunk has a slot for a coin cell (CR2032).
No on/off switch. Just take the battery out!

## Print Notes

Don't use PLA, because it isn't springy.
Rather than buy green PETG, I used transparent, and a green LED to light it ;-)

## Assembly
    
Either bend the LED pins in a contorted manner so that it just sits in place. Or...

Drill holes for wires in the battery connections.
Thread bare wires through the holes.
Drill 2 holes in the top of the trunk for a regular LED.
Solder the LED to the wires. The battery's internal resistance may be ok,
but you could add extra resistance if you want.

If the LED is too focused, scratch/sand the lens to spread the light.

When the wiring is done and tested, glue all the pieces together.
