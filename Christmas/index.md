# Christmas
## [Baubles](Baubles.foocad)

[![Baubles](Baubles.png)](Baubles.foocad)

## [FlatBauble](FlatBauble.foocad)

[![FlatBauble](FlatBauble.png)](FlatBauble.foocad)

## [NamedBauble](NamedBauble.foocad)

[![NamedBauble](NamedBauble.png)](NamedBauble.foocad)

## [SpringoSnoman](SpringoSnoman.foocad)

[![SpringoSnoman](SpringoSnoman.png)](SpringoSnoman.foocad)

## [SpringoTree](SpringoTree.foocad)

[![SpringoTree](SpringoTree.png)](SpringoTree.foocad)

