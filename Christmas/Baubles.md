Inspired by 3D scroll saw designs, such as the onese here :
[https://bijaju54.wordpress.com/tag/free-3d-christmas-ornament-scroll-saw-patterns/]

These take quite a while to render - over 3 minutes on my crusty old laptop.

Take two profiles (Shape2d) from an SVG document.
The first profile determines the general shape of the bauble. We use "revolve" to
create a "solid of revolution". But rather than have a solid object we create "slices"
The number of slices is given by @Custom slices.

The widths of these these slices is determined by the other profile from the SVG document.
The thickness of the slices is a constant ( @Custom thickness ).

Use inkscape to edit the profiles in the SVG document to create your own shapes.
Also consider combining this model with something else *inside* the bauble.
A tiny bird for example, as it it were in a bird cage. Or maybe an intial, so
each person has their own personal bauble.
