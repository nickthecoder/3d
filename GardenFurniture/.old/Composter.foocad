include General.foocad

/*

The corner pieces can either be screwed into all pieces, or just the center ring,
which would allow the box to be dismantled without unscrewing anything.
Note that the corners do not touch the ground, to prevent the end grain rotting.

Add a plastic liner, otherwise the wood may rot quickly.

*/
class Composter : General() {
    
    var length = 600
    var width = 600

    var ringCount = 5

    fun height() = (ww + gap) * ringCount - gap

    fun halfLumber() = Lumber( "halfLumber", ww/2, wt, "Orange" )

    init {
        // Using Wickes gravel boards.
        // https://www.wickes.co.uk/Wickes-Treated-Timber-Gravel-Board---19mm-x-150mm-x-2-4m-Pack-of-5/p/107142
        ww = 150
        wt = 19
    }

    override fun build() : Shape3d {
        val main = rings( lumber, length, width, gap, ringCount, false )
        val lid = lid( length + 80, width + 80, height() )

        val corner1 = halfLumber().cut( main.size.z - ww/2 )
            .rotateZ(90)
            .leftTo(main.right).backTo( main.back )
        val corner2 = halfLumber().cut( main.size.z - ww/2 )
                .darker()
                .rightTo(corner1.right)
                .frontTo(corner1.back)

        val corner = (corner1+corner2).translateZ(ww/2)

        return main + lid + corner.mirrorX().also().mirrorY().also()
           
    }

}
