I wanted a mini shed for long gardening tools, such as hoes, spades etc.
The left half of the shed has my lawn mower, and a sturdy shelf above for
my screws, nuts, bolts, nails etc.

The roof didn't follow this design, an instead it was made from a palette
that was getting in my way!

I used 3D printed hinges and sliding bolts, and so far no breakages ;-)

The doors use tent material. Will this stand the test of time???
