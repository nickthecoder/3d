A planter, which never rots, because the plants are placed in plastic containers
which are hidden from view by the light green trim.
I use these in front of my house, and swapping the plastic containers lets
me freshen the display without digging up the old display.
For example a display of spring flowering bulb can be replaced by geranimums.

