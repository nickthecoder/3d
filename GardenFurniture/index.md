# GardenFurniture
## [BikeShed](BikeShed.foocad)

[![BikeShed](BikeShed.png)](BikeShed.foocad)

## [ColdFrame](ColdFrame.foocad)

[![ColdFrame](ColdFrame.png)](ColdFrame.foocad)

## [Composter](Composter.foocad)

[![Composter](Composter.png)](Composter.foocad)

## [Container](Container.foocad)

[![Container](Container.png)](Container.foocad)

## [Cooler](Cooler.foocad)

[![Cooler](Cooler.png)](Cooler.foocad)

## [Demo](Demo.foocad)

[![Demo](Demo.png)](Demo.foocad)

## [Drainer](Drainer.foocad)

[![Drainer](Drainer.png)](Drainer.foocad)

## [ExampleNoobWoodworker](ExampleNoobWoodworker.foocad)

[![ExampleNoobWoodworker](ExampleNoobWoodworker.png)](ExampleNoobWoodworker.foocad)

## [General](General.foocad)

[![General](General.png)](General.foocad)

## [LargeContainer](LargeContainer.foocad)

[![LargeContainer](LargeContainer.png)](LargeContainer.foocad)

## [MiniShed](MiniShed.foocad)

[![MiniShed](MiniShed.png)](MiniShed.foocad)

## [Planter](Planter.foocad)

[![Planter](Planter.png)](Planter.foocad)

## [PlanterCupboard](PlanterCupboard.foocad)

[![PlanterCupboard](PlanterCupboard.png)](PlanterCupboard.foocad)

## [SeatContainer](SeatContainer.foocad)

[![SeatContainer](SeatContainer.png)](SeatContainer.foocad)

## [SeatPlanter](SeatPlanter.foocad)

[![SeatPlanter](SeatPlanter.png)](SeatPlanter.foocad)

## [TallColdframe](TallColdframe.foocad)

[![TallColdframe](TallColdframe.png)](TallColdframe.foocad)

## [Workbench](Workbench.foocad)

[![Workbench](Workbench.png)](Workbench.foocad)

