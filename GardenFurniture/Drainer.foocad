import static uk.co.nickthecoder.foocad.layout.v1.Layout2d.*
import static uk.co.nickthecoder.foocad.layout.v1.Layout3d.*

class Drainer : Model {
    
    var diameter = 270

    var ringT = 7
    var ringCount = 3

    var thickness = 2

    var gridGap = 10
    var gridT = 4

    var footD = 6
    var footGap = 30
    var footT = 10

    override fun build() : Shape3d {

        val profile = Circle( diameter / 2 )
        val footP = Circle( footD / 2 )

        val ringList = listOf<Shape2d>()
        val feetList = listOf<Shape2d>()

        for ( r in 1..ringCount ) {
            val ringD = diameter * r / ringCount
            val shape = Circle( ringD/2 )
            val feet = footP.translateX( ringD / 2 - ringT/2 )
                    .repeatAround( ringD * 3 / footGap )
            ringList.add( shape - shape.offset(-ringT) )
            feetList.add( feet )
        }

        val rings = Union2d( ringList )
        val feet = Union2d( feetList ).extrude( footT + thickness )

        val horiz = Square( gridT, diameter )
        val across = horiz.tileXWithin( diameter, gridGap ).center()  

        val vert = Square( diameter, gridT )
        val down = vert.tileYWithin( diameter, gridGap ).center()

        val grid = rings + (across + down) / profile


        return grid.extrude( thickness ) + feet
    }
}

