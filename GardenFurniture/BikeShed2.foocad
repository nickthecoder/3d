import static uk.co.nickthecoder.foocad.layout.v1.Layout3d.*
import uk.co.nickthecoder.foocad.compound.v1.*
import static uk.co.nickthecoder.foocad.along.v3.Along.*
import uk.co.nickthecoder.foocad.woodworking.v1.*
import static uk.co.nickthecoder.foocad.woodworking.v1.Woodworking.*

class BikeShed2 : Model {
    
    var size = Vector3( 1800, 600, 1100 )
    var extraHeight = 250
    var wheelSize = Vector2( 30, 50 )
    var wheelThreadDiameter = 30
    var tyreThickness = 5
    var wheelFlangeSize = Vector2( 4, 6 )

    var main = Wood( "main", 38, 22 ).color( "Green" )
    var half = Wood( "half", 38, 22 ).color( "Orange" )

    fun angle() = Math.tan( extraHeight / size.y )

    fun back() : Shape3d {

        val extra = main.thickness * Math.sin( angle() )
        val length = size.z + extraHeight - main.width * 2 + extra

        val top = main.cut( size.x - main.thickness* 2 )
            .label( "BackBottom" )
            .edgeDownAlongX()
            .leftTo( -size.x / 2 )
            .backTo( size.y / 2 )
            .centerX().darker()
            .topTo( size.z + extraHeight )

        val bottom = top.topTo( size.z )

        val vertical = main.cut( top.bottom - bottom.top )        
            .label( "BackVertical" )
            .backTo( size.y / 2 )
            .topTo( top.bottom )
            .repeatX( 4, (size.x -main.width-main.thickness*2)/ 3 )
            .centerX()

        return vertical +
            top + bottom
    }

    fun front() : Shape3d {
        val frontVertical = main.cut(size.z - main.width * 2)
            .label( "FrontVertical" )
            .frontTo( -size.y / 2 )
            .bottomTo( main.width )

        val center = frontVertical  
            .repeatX( 2, size.x / 2.5 )
            .centerX()

        val leftRight = frontVertical
            .leftTo( -size.x / 2 )
            .mirrorX().also()

        val bottom = main.cut( size.x )
            .label( "FrontBottom" )
            .edgeDownAlongX()        
            .leftTo( leftRight.right )
            .backTo( leftRight.back )
            .centerX().darker()

        return center +
            leftRight +
            // Top will need to be angled (planing/cutting along the grain).
            bottom.topTo( size.z ).also()
    }

    fun side() : Shape3d {

        val angle = angle()
        val angleDegrees = angle * 180 / Math.PI

        val front = main.cut( size.z - main.width*2 + 20)
            .label( "SideFront" )
            .rotateZ(90)
            .frontTo( -size.y/2 + main.thickness )
            .rightTo( size.x / 2  )
            .bottomTo(main.width)
            .darker()

        val back = main.cut( size.z + extraHeight - main.width )
            .label( "SideBack" )
            .rotateZ(90)
            .backTo( size.y/2 )
            .rightTo( front.right )
            .darker()

        val bottom = main.cut( size.y )
            .label( "SideBottom" )
            .edgeDownAlongY()
            .rightTo( front.right )
            .centerY()

        val topExtra = main.thickness * Math.sin( angle )
        // Will need trimming to size
        val top = main.cut( size.y + topExtra + main.width*2)
            .label( "SideTop" )
            .edgeDownAlongY()
            .centerY().translateY(topExtra/2)
            .rotateX(angleDegrees)
            .translateZ( (back.top + front.top) / 2 - main.width/2 )
            .rightTo( front.right )

        val across = bottom.translateZ( size.z / 3 )
            .topTo( back.top - size.z/3 ).also()

        return Compound().apply {
            + front
            + back
            + top
            + bottom
            + across
        }.build()
    }

    @Piece( printable = false )
    override fun build() : Shape3d {

        val bike = Cube( 1700, 400, 1020 )
            .centerXY()
            .previewOnly()

        return Compound().apply {
            //+ bike
            + back()
            + front()
            + side().mirrorX().also()
            
        }.build()

    }
}