Mini beer crates to hold batteries.

Inspired by https://www.printables.com/model/173788-customizable-stackable-beer-crate-for-all-types-of

Crates for AA, AAA and 9V are all the same height. The crate for 18650s is taller.
All are the same size in X & Y, and therefore stack on top of each other.

Glue the "stackable" piece to the base, which makes a stack of crates nice and snug.

My crates are a little bit taller than the originals. Mine also have writing on the sides.

## Print Notes

My first prints used 0.2 layer height with 0.4mm nozzle, but I them printed a couple with 0.3mm
layers, and they came out fine.

Choose different colours for each type of battery. Use black "stackable" pieces for all of them.
