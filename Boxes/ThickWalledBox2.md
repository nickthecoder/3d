A rounded rectangular thick walled box which snaps together with an interferance fit of lips.
The ouside and inside of the box are smooth.
    
Comprised of a bottom and a top, and optionaly middle sections, which are hollow
tubes (with the same lip connectors).
You can include as many middle sections as you like, and either keep them
free, or glue them to the bottom.
    
An upgraded version of ThickWalledBox, but changed the name so that other scripts
can use the old version still.
    
## Print Notes

I highly recommend trying colour transparent PLA. It give an amazing striped effect
from the infill pattern.

Use 2 perimeters, and 10% infill

The corners of the lip are over-extruded when I create a perfectly square box, so
I have to cut a little with a knife. YMMV.
