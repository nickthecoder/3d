A simple box, with wooden runners to house my B&Q component organiser boxes.
https://www.diy.com/departments/20-compartment-compartment-organiser-case/1345784_BQ.prd

NOTE. I will make one of the boxes fixed in place, with full extension
drawer runners, so that I can use it as a shelf (as somewhere to place each
box when it is removed from the cabinet).
