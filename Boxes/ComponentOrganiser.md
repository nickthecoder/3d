A storage container designed for my electronics components (but will
be suitable for other uses too).

Each drawer contains a number of tubs (inserts).
The inserts can be different sizes, but are all integer multiples of the
smallest insert.

I have chosen to use the same size inserts as used by
Raaco's "A Series", because I already have some of these,
as well as other brands which use the same "standard" size.

Unlike the Raaco system, I don't want lids on each drawer.
I would like to open the drawer fully, so that I can get to all of the tubs
without the drawer falling out.

I want a mechanism to prevent the drawer being pulled out too far.

Each drawer front should have a place for a label.

The inserts have small feet, which fit into circles in the drawers.
The feet ensure the tubs line up correctly, even when some tubs are missing.
To print the feet, we probably need to print the tubs upside down;
the base is printed using bridging - you will need good part cooling!

Many cabinets can be glued together to form a large array.

See Racco's solutions :
    https://www.raacostorage.co.uk/PBSCCatalog.asp?CatID=2279874
    https://www.raacostorage.co.uk/a-range-inserts-c102x2279884
FYI. The "55" refers to the depth (y-axis) of the smallest insert.
This is also referred to as the "A" range.

You could fix the cabinets to the wall using screws, but the existing model
doesn't help you out! Consider tweaking the code to add "keyholes" on
the cabinet and adjust the "extra" space so that the drawers still fit.

Print Notes
-----------

I use a "coarse" profile, with 0.3mm layers and high speed moves.
(except for the 1st layer which I like to take slow).
