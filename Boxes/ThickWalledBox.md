A thick walled box which snaps together with an interferance fit of lips.
The ouside and inside of the box are smooth.

Comprised of a bottom and a top, and optionaly middle sections, which are hollow
tubes (with the same lip connectors).
You can include as many middle sections as you like, and either keep them
free, or glue them to the bottom.

## Print Notes

I highly recommend trying coloured transparent PLA. It give an amazing striped effect
from the infill pattern.

Use 2 perimeters, and 10% infill
