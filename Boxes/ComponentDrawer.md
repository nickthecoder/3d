Creates drawers which can be attached to the bottom of a shelf / cupboard.
The drawers have a grid for "inserts" from ComponentOrganiset.foocad.
The drawers open/close with the help of DrawerSlide.foocad.

Glue a pair of "inner" slides to the drawer. When the glue is dry, attach the middle and outer
and screw the outer to the shelf/cupboard. Ensure that the slides are parallel.

