A box, with thin walls, but thicker there the lip of the lid meets the main body.

The box can be built in 2, 3 or more parts.
You will always need 1 "top" and 1 "bottom", and you can add 0 or more "middle"
pieces.

"slack" is quite and important @Custom parameter, and will need fine tuning for
your partiuclar printer. Too small, and the lid will jam on too tightly.
Too large, and the lid will be horribly loose.
Only the "top" piece uses "slack", so if you need to adjust it, only the "top"
piece needs to be re-printed.

The shape of the box is arbitrary (override profile() method).

To get all the other profile sizes (for the lip, inside, chamfer etc),
I currently use "scale". This ensures it works with any profile you want,
but has the disadvantage that it doesn't produce perfectly consistant width
lips.

The "better" solution would be to use Shape2d.offset(n), but this can produce
non 1-to-1 correspondance between the points that make up sucessive profiles,
and ExtrusionBuilder does a very bad job sometimes! A future version of FooCAD
may fix this, and then Shape2d.offset will be better than Shape2d.scale.
