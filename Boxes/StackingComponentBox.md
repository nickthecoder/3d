A stacking box, which will contain standard sized component organiser inserts
from ComponentOrganiser.foocad.

## Print Notes

### main

Use many perimeters, which is better than it doing
lots of little back and forths when it gets to the thick bit at the top.

### lid

10% infill, and 2 perimeters is fine.
