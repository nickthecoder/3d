The X stepper motor overhangs the sides of the printer,
and one day I left a drinks can under it.
When the gratry lowered the gantry jammed against the can, making a dreadful sound.

This item prevents that from happening again.
The dovetail is glued to the side of the printer, and the box fits in the dovetail.
Now I can't accidentaly leave something there.
