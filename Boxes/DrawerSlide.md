I want a drawer slide for some component organisers containing my electrical components.
So the drawer needs to take very little weight, but I do need them to be fully extendable
(i.e. the entire drawer+ comes out)

Fine tune the [slack] parameter. Do this while using the same ModelExtensions and print settings
as you will for the final product, but print a short (50mm) rod.

## Print Notes

Use a high perimeter count, so that the sides of the middle and outer bars are solid.

Use the Ears ModelExtensions to help bed adhesion.
Use Rotate ModelExtension by 45° (place it *below* the Ears extension) for slides longer
than your build plate.

## Version History

Now using chamderedExtrude as my printer (or my settings) cause right andgle corners to have an extra glob
of plastic, which ruins the fit, and needs touching up with a knife.
There is still a 90° corner at the internal "stop" (or middle and outer pieces).
