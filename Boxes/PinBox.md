A box, which slides open, with catches at the sides to prevent it
opening by accident.

Includes a slope to help get pins out by dragging them up the slope.
