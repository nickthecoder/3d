A tissue box cover, for those who don't like to look at cardboard boxes!
Print the main body using "vase" mode, and make the walls thicker than normal
by increasing the "Default Extrusion Width" to say 200%
