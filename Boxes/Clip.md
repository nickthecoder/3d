A clip to keep a hinged box closed.

Glue the two orange piece to a hinged box, so that it can be closed

## Notes

I've not tried this (yet).

My original hinged box had the orange piece built-in,
which don't work very well. Alas, I never got around to
trying this solution.
