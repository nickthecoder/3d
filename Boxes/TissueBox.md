A square tissue box with rounded corners, chamfered top/bottom edges
and a pattern on each face.

Printed in two parts, the main body, and a lid. I considered making the "lid"
at the bottom, but then picking up the box may let the tissues fall out the bottom!
Also, I like printing the top in a different colour to the main body.

The default size fits Wilko's own-brand tissues.
Kleenex are smaller, not square and more expensive! Not recommended ;-)

The lid should have an "interferance" fit with the main body. Adjust
"slack" appropriately. If it is too loose, add a little masking tape
on the lid's lip. Too loose and the lid will lift each time you take a tissue.

## Print Notes

I always have bed adhesion problems with largish boxes. The corners always lift.
I used a 1st layer temp of 205 C, 65 C bed temp for all layers,
a 10mm brim, as well as 10 mm ears.
Using a PEI surface with glue stick.
It worked - no lifting at all.

## Possible Improvements
    
1. Make the lid clip on
2. Add a "frame" around the pattern - a rectangular indent/outdent.
