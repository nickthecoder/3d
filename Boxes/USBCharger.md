I have numerous wall wart going spare and I want to convert one or two
into USB chargers, with the aid of a buck module to step the voltage down
to 5V.

Inside is a simple circuit board with 1 to 4 USB sockets. Each has their
data pins shorted (which indicates this is a charger only, and the device
can draw high current)

Pinout for the sockets can be seen here :
[https://en.wikipedia.org/wiki/USB_hardware#/media/File:USB.svg]

Add a short between D+ and D- to indicate this is a "charge only" port.

My buck module has a max output current of 3A.
A Raspberry Pi 4 need 3A, so may not be suitable.
A Pi 3 needs 2.5A, And Pi 1&2 less than 2A.
