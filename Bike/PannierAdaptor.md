_Post Processing_

The hole needs to be drilled out, using a 5mm drill bit.
(I can't make the hole any smaller in the model).

Measure the distance between the two `bagClip` pieces when attached to the bag.
Glue the blocks to the box, then drill the hole through the box.

_Custom Pieces_

The box's ribs aren't the same size all the way round, so I need custom version of piece
`boxBlock`.

#larger#  : For the right side of the bike. It is ribs are deeper there.
