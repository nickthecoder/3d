See FlatBed.foocad.

I have a heavy-duty plastic box, that I want to attach to the back of my bike.

_boltKnob_

The box is attached attached to the plywood with 4 bolts.
This piece makes the bolt head much bigger, and easy to screw/unscrew by hand.
The bolt is glued to this piece.

_boltKnob2_

The bolts the hold the flat bed to the pannier rack.
