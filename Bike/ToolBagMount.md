A two part clip, with holes for two M6 threaded inserts.
These attach a small toolbag to the handlebar stem of my bike.

The stem is tapered, so I need 2 different sizes (34 and 36 mm internal diameter).
They feel very secure.
