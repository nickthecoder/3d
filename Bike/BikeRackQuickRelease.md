A quick-release mechanism for the box which sits on top of my bike's pannier rack.

The transparent bar represents part of the pannier rack.
The transparent cuboid represents the wooden board at the bottom of the box.

The green block screws into the wooden board.
The main cam (yellow) is screwed to the green block using a plastic bolt (orange).

The bold has a wide shoulder, so that when it is screwed tight,
the cam is still free to rotate.

To ensure that the cam stays in the locked position, I adjust
rackDiameter (which changes the height of the green block),
so that the cam is tight (it needs to bend ever so slightly,
squeezing against the rack's bar).

The protruberance on the cam acts as two stops, which limit its motion to 90°.

Piece `runner` stops the box lift from the back (where as the other pieces attach at the sides).
The box slides under the `runner`, and then the quick-release mechanism is locked.
These are smal, because there is a piece of wood there already. Without the wood,
the `runnerBlockSize.z` would need to be slightly larger than `rackDiameter`

# Expected Point of Failure

The bolt will shear along layer lines. A small piece to replace :-)

# Print Settings

## cam

I tried TPU (A95), but that was too flexible, and the box could be pulled up without
releasing the cams.
My 2nd (and final) set, I chose ASA (for no particular reason).
PLA is NOT a good choice as it is stiff and breaks easily.

The thickess (camSize.z), the flexibility of the material and the amount of infill & the number of top/bottom layers
will affect overall flexibility of the final piece.

All other pieces can be any non-flexible material.

## bolt

Print solid as this is weak along the layer lines.
