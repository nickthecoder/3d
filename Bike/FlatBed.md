See also 
## Requirements

* Carry lots of shopping on my ebike.
* Carry bulkier items by tying down to a flat bed (e.g. bags of compost)
* Easy to take on/off without tools.

## Solution

Attach a piece of plywood to the pannier rack.
It slots in place, and is secured by three bolts.
The bolt head is plastic, and overlaps the pannier rack.
They are weak(ish), and hopefully, they will snap off if the bike is knocked over.
I'd prefer these reprintable bolts to break, rather than the box!

The box has a threaded inserts at each corner.
The plywood base has holes in these positions, so the box can be screwed to the plywood.
To make positioning the box easier, I also added some plastic guides to the
top surface of the plywood.

Note. I ended up using plywood which was sturdy enough without the extra battonning
around the edges. So only the three thinner pieces of wood remain.

Before entering the house, I can unscrew the 3 bolts, and lift the box and the base
together. The box will only be unscrewed from the base when I buy compost, or other
bulky items.
