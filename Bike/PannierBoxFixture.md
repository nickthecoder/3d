Helps attach a "heavy duty" plastic box to my bike's pannier.
If the box is hit (or the bike falls over), it is likely that the clips will break.
I prefer the clips break than the box.
This has happened more than once, when my bike fell over. I keep spares on me!

The bottom of the box is not flat, and instead has reenforcing fins.
So for these fixtures to touch the bottom of the box, there is a `block` built into the model.

The shape of the clip is similar to the [PannierClip], but in this case,
I've created it progamatically, rather than using an SVG diagram.

I'm using M5 brass inserts, which are melted into the plastic with a soldering iron.

## Print Notes

PETG is preferable, as it has a bit of spring to it.

PLA is more rigid, and therefore breaks more readily.

## Version

V1 I made the "block" larger than needed.