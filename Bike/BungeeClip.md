A clip for bungee cord, to attach to my bike's pannier.
The basic shape was copied from ../Bike/pannierClip.svg

Each clip has three holes. Use them as you see fit.

For a simple cord, only 1 hole is required.

## Notes

They get tangled very easily!
I no longer use them, as I've changed the way I use my pannier rack.
