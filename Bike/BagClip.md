A hook for the box on the back of my bike, for hanging bags on the sides/back
if I need extra space for shopping.

The box has ribs at the top, so the hook has to clear those ribs.

There's an option for a bolt, which ensures the bag cannot jump out of the hook,
and the hook cannot jump off the box
The end of the bolt sits between the ribs at the top of the box.

_Design Notes_

I initially left the end of the bolt unthreaded, but changed my mind.
The threads make the bolt stronger, and having threads along the whole length
allows the bolt to stay in place while placing the bag on the clip.

_Strength_

My test print (with sparse infill) is much stronger than necessary.
The bag handles (or the box) would break before the clip.


_Print Notes_

The clip lifted off the bed on my test print, so I resorted to adding a brim :-(
Even a small lift can deform the threaded holes, and ruin the print.

My test prints used a 0.3 layer height with a 0.4 nozzle.
This was slightly too agressive for the overhangs in the threaded holes,
but the thread still worked, but was stiff until screwed and unscrewed several times.

_bolt_

Print with a brim, as it is tall and thin.
