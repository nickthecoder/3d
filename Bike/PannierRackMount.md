I want to attach a box, OR a flat piece of plywood to my pannier, and swap them easily.
When buying groceries, I use the box, but bulky items will use the "flat bed" plywood.

I never printed this, as I opted for a different solution.

## Print Notes

I suggest adding "ears" to prevent warping.
