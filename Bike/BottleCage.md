I have a strange shape frame on my bike, and I don't want to drill into it to fix a
bottle cage, so here's my design to wrap around the bike's rectangular section.

Use M4 machine screws. On my printer, the holes are just the right size to self tap with the screw.

