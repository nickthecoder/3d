The clip on my Aqua 14 pannier broke (on the first ride!)

This is a simplified replacement without the small springy part in the middle, which
seems irrelevant to me (it clips on firmly without it).

There is an obvious weak point where the screw attaches (especially in the original).
The latest version is thicker than the origianl to try to compensate.

I copied the curve by tracing a photo, and I think it is pretty close to the original.

## Print Notes

Use a generous brim (the model is very small where it touches the bed).
I used 4 perimeters and 30% infill. Maybe bump up both, given that the design as a weak point!

PLA is fine, but maybe PETG would be better (stronger and springier).

New (fatter) version : White PLA 5 perimeters 30% infill
    Black PLA : 8 perimeters 30% infill

## Versions
    16/2/2023 Made it fatter than the original, as I've had 2 failures of the old design.
