I bought a Kryptonite bike lock, without the mounting bracket.
The bracket wouldn't have fitted my bike anyway.

So 3d printing to the rescue ;-)

The holes are designed for M4 machine screws.

## Notes

I used this for years now, without an problems :-)

If I were to print this again, I would make it stronger where the screws are.

Also, the model allows an M4 bolt to screw into the plastic, but I ended up going
straight through, and using a nut. So I could have made both holes the same (larger) size.

## Print Notes

Print partA and partB or just the "default" piece.

partB may require a brim, as it is tall with very little area on the bed.
