A chess set, made mostly from "solids of revolution".
(All except the knight, which is in the shape of a double G)
Get the joke?

I've never printed it. The Knight needs to be taller.

Also, if I did print it, I'd make cylindical hollows
in the bases to weight the pieces with coins.
