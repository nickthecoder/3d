A gears have teeth which mesh together arranged around a circle.
However, I want to add teeth around ANY shape.
So lets consider perfectly meshing teeth along a STRAIGHT LINE.

   
   ┌───────────────────┐
   |_     PART A     __│
	 \  /\  /\  /\  /
      \/  \/  \/  \/

        /\  /\  /\
    ___/  \/  \/  \_____
   |       PART B       |
   └────────────────────┘   

We can then warp the geometry so that these teeth are around a circle, rounded rectangles,
ellipses...

So our primative tooth profile (shown as a triangle in the diagram above) is for teeth
along a straight line.
FYI, A circular gear running along a straigh is called a
[Rack and Pinion](https://en.wikipedia.org/wiki/Rack_and_pinion).

Our basic tooth profile is that of the rack, and the warping it around a circle
will form the pinion.

Triangles aren't a good tooth shape though.

Firstly, the sharp corners don't play well with 3D prints.
So we should at the very least round the corner.

Secondly, as the gears rotate we want the force to be constant, and the motion to be smooth.
What we need is an [Involute Gear](https://en.wikipedia.org/wiki/Involute_gear).

A rounded triangle would give very variable force. At the rounded part of the travel
there would be nearly zero force
	