A tricky puzzle. Place the pieces in a 3x3 grid.

Flipping the pieces over isn't allowed 
(so I suggest swapping colors half way through).
