Takes a (roughly) spherical object, and cuts it into an eighth, with a flattened
base, so that it can be printed wihout overhangs.

You can use this within your own models in two ways :

1. extend Sphere8 and override the sphere method
2. Call the static method Eighth8.eighth( shape, diameter, flatten )

