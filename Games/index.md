# Games
## [Avagojo](Avagojo.foocad)

[![Avagojo](Avagojo.png)](Avagojo.foocad)

## [Backgammon](Backgammon.foocad)

[![Backgammon](Backgammon.png)](Backgammon.foocad)

## [BallHolder](BallHolder.foocad)

[![BallHolder](BallHolder.png)](BallHolder.foocad)

## [BallHolder2](BallHolder2.foocad)

[![BallHolder2](BallHolder2.png)](BallHolder2.foocad)

## [BallHolderRails](BallHolderRails.foocad)

[![BallHolderRails](BallHolderRails.png)](BallHolderRails.foocad)

## [CardShufflerSpindle](CardShufflerSpindle.foocad)

[![CardShufflerSpindle](CardShufflerSpindle.png)](CardShufflerSpindle.foocad)

## [ChessPiece](ChessPiece.foocad)

[![ChessPiece](ChessPiece.png)](ChessPiece.foocad)

## [ChessSaw](ChessSaw.foocad)

[![ChessSaw](ChessSaw.png)](ChessSaw.foocad)

## [ClubsHolder](ClubsHolder.foocad)

[![ClubsHolder](ClubsHolder.png)](ClubsHolder.foocad)

## [ExampleJigsaw](ExampleJigsaw.foocad)

[![ExampleJigsaw](ExampleJigsaw.png)](ExampleJigsaw.foocad)

## [FloatingSphere](FloatingSphere.foocad)

[![FloatingSphere](FloatingSphere.png)](FloatingSphere.foocad)

## [FloatingSphere2](FloatingSphere2.foocad)

[![FloatingSphere2](FloatingSphere2.png)](FloatingSphere2.foocad)

## [Freeplunk](Freeplunk.foocad)

[![Freeplunk](Freeplunk.png)](Freeplunk.foocad)

## [GameTiles](GameTiles.foocad)

[![GameTiles](GameTiles.png)](GameTiles.foocad)

## [GiftTube](GiftTube.foocad)

[![GiftTube](GiftTube.png)](GiftTube.foocad)

## [HexBall](HexBall.foocad)

[![HexBall](HexBall.png)](HexBall.foocad)

## [JigsawSuits](JigsawSuits.foocad)

[![JigsawSuits](JigsawSuits.png)](JigsawSuits.foocad)

## [JugglingBall](JugglingBall.foocad)

[![JugglingBall](JugglingBall.png)](JugglingBall.foocad)

## [JugglingClub](JugglingClub.foocad)

[![JugglingClub](JugglingClub.png)](JugglingClub.foocad)

## [LEDJugglingBall](LEDJugglingBall.foocad)

[![LEDJugglingBall](LEDJugglingBall.png)](LEDJugglingBall.foocad)

## [LazySusanSpacer](LazySusanSpacer.foocad)

[![LazySusanSpacer](LazySusanSpacer.png)](LazySusanSpacer.foocad)

## [NinePieceJigsaw](NinePieceJigsaw.foocad)

[![NinePieceJigsaw](NinePieceJigsaw.png)](NinePieceJigsaw.foocad)

## [Penrose](Penrose.foocad)

[![Penrose](Penrose.png)](Penrose.foocad)

## [Penrose2](Penrose2.foocad)

[![Penrose2](Penrose2.png)](Penrose2.foocad)

## [PuzzleBox](PuzzleBox.foocad)

[![PuzzleBox](PuzzleBox.png)](PuzzleBox.foocad)

## [PuzzleOfEvil](PuzzleOfEvil.foocad)

[![PuzzleOfEvil](PuzzleOfEvil.png)](PuzzleOfEvil.foocad)

## [ReactionBall](ReactionBall.foocad)

[![ReactionBall](ReactionBall.png)](ReactionBall.foocad)

## [SVGJigsaw](SVGJigsaw.foocad)

[![SVGJigsaw](SVGJigsaw.png)](SVGJigsaw.foocad)

## [ScrabbleLazySusan](ScrabbleLazySusan.foocad)

[![ScrabbleLazySusan](ScrabbleLazySusan.png)](ScrabbleLazySusan.foocad)

## [Solitaire](Solitaire.foocad)

[![Solitaire](Solitaire.png)](Solitaire.foocad)

## [Sphere8](Sphere8.foocad)

[![Sphere8](Sphere8.png)](Sphere8.foocad)

## [Sphericon](Sphericon.foocad)

[![Sphericon](Sphericon.png)](Sphericon.foocad)

## [SpinningTurtles](SpinningTurtles.foocad)

[![SpinningTurtles](SpinningTurtles.png)](SpinningTurtles.foocad)

## [Spiro](Spiro.foocad)

[![Spiro](Spiro.png)](Spiro.foocad)

## [Super7](Super7.foocad)

[![Super7](Super7.png)](Super7.foocad)

## [ThickWalledBox](ThickWalledBox.foocad)

[![ThickWalledBox](ThickWalledBox.png)](ThickWalledBox.foocad)

## [TwoPieceSphere](TwoPieceSphere.foocad)

[![TwoPieceSphere](TwoPieceSphere.png)](TwoPieceSphere.foocad)

## [WhistlingTop](WhistlingTop.foocad)

[![WhistlingTop](WhistlingTop.png)](WhistlingTop.foocad)

## [WordGames](WordGames.foocad)

[![WordGames](WordGames.png)](WordGames.foocad)

