Playing tiles for various games, including Qwirkle.
[https://en.wikipedia.org/wiki/Qwirkle]

These can be weighted with a penny (or a washer). See Custom [weight].
All plastic tiles feel cheap, the added heft makes a big difference.

## Versions

I origionally used hearts,clubs,diamonds,spades symbols.
However, symetric symbols are better.

## Colors Used

In case reprints are needed.

* 3DQF Matt Black (Base)
* 3DQF Sky Blue,
* Filamentive Cosmic Gold
* Copymaster Bloody Red,
* PrimaSelect Nebula Purple
* Cheap (unbranded) Pink
* 3DQF British Racing Greeen
