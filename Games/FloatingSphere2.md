Inspired by Maker's Muse :
[https://www.youtube.com/watch?v=7G5WpeJ1iG4]

A magic trick. Wind thread around both parts of the pulleys, with the ends
exiting opposide poles of the sphere.

Pull to make the sphere rise up, release the tension to allow ot to fall under gravity.

There are several ways you could join the two halves. Magnets, glue, elastic bands.
I have a slice of an old bicycle inner tube which works well (but is tricky to get on).

Note. It's funny how much more convoluted Maker's Muse's version is.
Using plastic clips to join the halves together caused all kinds of problems, which
the magnets of mine bypasses. Also, his has a metal bearing, as did my first version!
But playing with the first print convinced me that it wasn't needed. Nor is there
any need for an axel for the pulleys. So we end up with 3 basic parts.
    
I printed this on a budget printer (an Artilery Genius) using 0.2mm layers, and
was able to get it running smoothly after a few minutes with a craft knife!

## Construction Notes

Test fit the three parts, carefully removing any blobs of plastic which prevent
smooth rotation.

Drill 2 small holes in the pulley from each groove into the central hole.
The holes must be big enough so that a tied piece of string will fit through.

Poke the thread into one of the holes, and use tweezers to pull it from the
central hole. Do the same with the other end of the string for the other pulley.
Tie the ends together - the knot should be in the central hole.

Pull the string, so that the knot goes though one of the small holes.
Snip the knot off - your thread is now correctly threaded :-)
Wind half of thread onto the large pulley.
If you wish, you can use a dab of hot glue to prevent the thread for coming
undone.
Tie each ends to a ring (Use the plastic ones or your own jewelry!

Assemble the three pieces, and keep them together temporarily with elastic bands.
Check that the mechanism works

If the pulley is free to move in both halves, but jams when the three pieces are
put together, you need a slight gap between the two halves. A piece of paper (or two)
cut to shape will do nicely. You could use double sided tape ;-)

Once everything is working, you could replace the elastic bands with a few tiny
spots of super glue. Keep well away from the pulley.
