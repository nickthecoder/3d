A two piece hollow sphere that is joined by a separate screw thread in the middle.
Space inside for adding LEDs and battery etc. Or alternatively, fill with plaster/concrete
for a weighty juggling ball.

The overhang is solved by creating a flat spot in the interior, so that the
interior poles use bridging before the angle gets too steep.

You can change the position of this flat spot using the @Custom overhang.

You can override outerSphere() to make a custom sphere, e.g. with writing
or other patterns.

The default size is a "comfortable" size for an adult hand, but larger balls
will give a better performance.

## Print Notes

I added a 3mm brim for the hemisphere, because one of the prints detatched.

Use transparent PLA for a milky white translucent finish... Coloured LEDs
will show up nicely.

If you use PLA, and leave the ball TIGHTLY screwed together for long periods,
will the threads deform over time???

You can get a milky white finish from transparent PETG if you lower the
extrusion temperature, but this will also reduce the layer strength.
