A weird shaped juggling ball.
I recommend PLA for the squishy feel.

Either print 2 "half" pieces, or a "middle" and two "ends".
Using the later, you can choose the width of the middle via "middleRatio".
