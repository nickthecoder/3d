First ("final") game printed with :
    Main            : Copy Master Bloody Red PLA ???
    Playing Surface : Filamentive Light gray PLA
    Base            : 3DQF Navy Blue PLA

Second ("alternate") game printed with :
    Main            : Prima Select Glossy Nebula Purple PLA
    Playing Surface : 3DQF Pastel Purple PLA
    Base            : 3DQF King Fisher Blue PLA

Was having trouble with the maze walls, so ended up adding a brim, but I think the issue was
incorrect Z offset.
First version used a 0.4mm nozzle, but I'm trying 0.6mm for the second,

