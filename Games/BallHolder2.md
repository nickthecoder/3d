A wall mounted holder for 3 juggling balls. A companion to the ClubsHolder.

Print 1 "base" and three "ring" pieces. The rings can be TPU for a rubbery feel.
Glue with cyanoacrylate (AKA superglue).

The design is flawed! Over time, it has sagged.
In hindsight there should have been some kind of triangulation for strength.
Surprisingly it has never broken, despite being knocked many times.

