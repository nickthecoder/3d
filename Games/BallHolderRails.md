Holds juggling balls on two wooden rails. The brackets are attached to the
wall by screws.

NOTE. There is a keyhole cutout to attach it to the wall, but to aid printing,
it is hidden from view - you will need to cut 1 perimeter of plastic to reveal it.
