A four piece jigsaw puzzle that is very challenging!
I can confirm that both puzzles are possible.
The "hard" side is particularly satisfying - don't give up!

## SPOLIER ALERT

(Don't read this if you don't want any clues)...
If you write a program to solve this by brute force, it will find the
"easy" solution, but probably won't find the hard one.
If so, your program is wrong. What assumptions is it making?

Read more about the puzzle and its origins here :
[https://blog.khanacademy.org/how-wooden-puzzles-can-destroy-dev-teams/]
