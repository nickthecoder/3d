Create pieces and boards for various word games, such as Scrabble, Bananagram and Raid Word.
    
## Print Notes

The tiles should be printed in two colours
(the printer will pause when the filament needs changing)

I recommend printing a second set using a different colour, so that it is easy to play
scrabble with one set, as well as other games which need more than one set.

## Scrabble

1 set of tiles.

It took about 3hrs 30mins to get to the "change filament" stage for half a set of tiles.

* 9xscrabbleBoard
  I Used light grey
  Use a brim and/or ears to prevent lifting.
  Let the bed full cool, otherwise you may bend it when removing.

* 8xtws (triple word score) in RED
* 17xdws (double word score) in PINK
* 12xtls (tripple letter score) in DARK BLUE
* 24xdls (double letter score) in LIGHT BLUE

Glue these in place on the boards        

### The box.

I used 20mm ears 0.6mm height with ribs and 6mm brim.
2 Perimeters using a translucent filament gives a pleasing effect
(the infill makes visible patterns)

## Notes
        
Here's the guide I used to sew a plain draw string bag :
[https://www.youtube.com/watch?v=0OuhGPFVlro]

Cut 3cm wider + 4cm at the top Fold 1cm, then another 3cm for the strings.

## Raid Word

Use 5+ perimeters. Otherwise the rim around the tile slots may be nearly an exact multiple
of perimeter width, and it then prints tiny dots, and the retractions may grind the filament,
causing the print to completely fail.

* 2 sets of scrabbleLetters
* 4x raid5
* 24x raid4
* 24x raid3
* 8x raid2

## Bananagram

* 2 sets of scrabbleLetters

