SVG Rules
---------

You can create mazes by drawing an SVG diagram of the maze, using Inkscape.
I strongly recommend you `copy` an existing maze, rather than starting from scratch,
because it has sensible document properties, such as a grid.

1. All objects must be ungrouped, and in the "default" layer.

2. Every object must have an ID which specifies what type of object it is.
    Use Ctrl+Shift+O in Inkscape to edit an object's ID.
    There are two pieces which MUST be present :

    1. `playingArea`
    2. `ramp` : A 4x1 rectangle.

    Optional items with fixed IDs :

    1. `solution` : A path, showing the solution of the maze.
    2. `center` : A 3x3 circle for the center of the maze.
        This may have a suffix of "N", "S", "E" or "W" to indicate the direction of the exit.

    The remaing IDs must be prefixed as follows :

    1. `hole` : The text after `hole` may be used to print the hole numbers on the floor???
    2. `path` A wall (where X is any 
    3. `post` A post to support the playing area and the glass.

2. All wall nodes must be at a grid intersection.
    (Enable Snapping -> Nodes -> Cusp nodes)

3. All other objects must have their bounding box corners at a grid intersection.
    (Use `Alt` while dragging, and enable Snapping -> Bounding Boxes -> Corners).

4. Walls may be a path containing many line segments. However, I find it easier to
    make each wall a single line (either horizontal or vertical).
    Warning, multi-line paths can cause issues when using a large wall chamfer.

4. All other attributes, such as color, line thickness etc. are ignored.

Printing the Maze
-----------------

I've given the option of printing the floor and the maze walls as one piece, but I prefer
to print them separately. The floor is printed playing surface downwards, and is therefore
very smooth.

Hole Numbers
------------

The piece named "holes" shows the positions of the holes (as a 3D model), but it also
prints an SVG document to the log. Copy/paste it into a new file (holes.svg), and
then print it using a 2D printer.


