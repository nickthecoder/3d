A wall mounted holder for three juggling clubs.
It should be fairly straight forward to adapt this for five clubs (or more).

Each prong is reinforced with a metal rod. The default size is for a 6mm rod.

Uses two keyhole slots to secure it to the wall.

To get the gap and angle correct for your clubs, print out the prongs first,
and then fix them with the metal rods to a piece of scrap wood.

You need 1 "back" piece, and 4 "prong" pieces. The prongs should be printed with
TPU for that rubbery feel.
    
However, "back" doesn't fit on my printer, so I had to break it into two piees,
with a small cuboid to keep them aligned.
So in this case use piece "backPieces" or "backA", "backB" and "connector" if
you prefer printing them all separately.

Note the position and size of the prongs should allow the clubs to fan out
nicely, without touching each other, and only the center club should
hang straight downward.
