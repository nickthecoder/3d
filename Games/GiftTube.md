Twist th two pieces to open. Inside there is a tiny compartment.
Roll up a note for a money gift idea?

The outer piece has a nub of plastic, which fits into a groove
in the inner piece. This groove is a maze.  Twist and slide the outer case
to navigate the maze. The tricky part is that the maze is hidden from view.
