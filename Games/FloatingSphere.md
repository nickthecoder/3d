Inspired by Maker's Muse :
[https://www.youtube.com/watch?v=7G5WpeJ1iG4]

A magic trick. Wind thread around both parts of the pulleys, with the ends
exiting opposide poles of the sphere.

Pull to make the sphere rise up, release the tension to allow ot to fall under gravity.

Note. Maker's Muse is much more convoluted, mostly because he didn't use magnets to
attach the two halves. This snowballed into lots of design constraints, such as the
need for a separate shell, and printing the inside parts "upside-down".

## To Do

Consider adding a place for a metal insert at the north and south pole, for a tiny
tube, or even just a replacable plastic insert (which will wear quickly).
