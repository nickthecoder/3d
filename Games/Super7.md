Super 7 is a board game based on noughts and crosses - but much better!
    
Read the rules here (also pasted at the end of this script):
[http://nickthecoder.co.uk:8081/instructions/superSeven.html]

For a LONG game, print 9 complete sets, and arrange them in a 3x3 grid.
First to win 3 games in a line wins (or a total of 5 games) ;-)
I'm kidding, but also curious to know how this super super 7 game would pan out!

## Print Notes
    
Print lots of coins (40 ish) of each colour. I chose blue and red.
4 or 5 bigCoins of each colour (the game is over if you create 5 big coins).

If you prefer, you could print only the small coins, and no bigCoins.
When a tile is won, fill the whole tile with the tile winner's coins.

Each of the tiles (numbered 3 to 11).
Print them three at a time using tile1..3, or all in one go using piece name "tiles".

The printer will pause when you need to change filament (twice).
I used white, grey then black

You also need 2 dice.

## Notes

The box has enough space to shove the coins in willy nilly, but you can also fill up
each tile, add the bigCoin on top, and then slide it into one of the slots.
The coins do NOT fall out ;-) So the reamining space is just for the dice.

There are two logos for the top of the box. The original game was called Super7.
But that name is now used by something quite different. The box and tiles reminded me
of After Eight chocolates, so I thought it amusing to call the game Around Seven.
