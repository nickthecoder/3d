A high quality Juggling club, with a wooden dowel core (default size is 18mm).

The shape od the club comes from a 2D profile in an svg diagram, which you can edit
using the free Inkscape program.

If you want a "basic" clubs, just go out and buy them!
The beauty of 3D printing is customisation, so get customising ;-)

Change the length to suit the length of your arms - one size does NOT fit all.
Change the shape of the club. Go wild - jazz up the profile into freeky curves!
Edit the svg document using inkscape (a free vector drawing program).

And obviously, you can choose your color. If your printer lets you change
filament part way through a print, then why not create striped clubs!
I suggest not transitioning from one strong colour to another, as the mixed
colour may be nasty. Instead add a stripe of white or black between each colour
change. If you want consistant clubs, make a note of the times of each change,
or print all three clubs at the same time.

I originally split the head into two parts (headA and headB), but I've changed the
profile of the club so that "head" can fit on my printer in one part.
The "stop" is now longer than it used to be.
    
It screws together, but all screws are hidden from view. To take the club apart
you will need to remove the glued parts "cap" and "endB". If you used CA glue,
a sharp knife should work without damaging the parts too much.
But I consider these pieces "consumerbes", because they will take lots of knocks.
    

## Pieces

* one - The whole club in one piece (not recommended)

* head - The main part of the club. Currently just over 200mm
* headA - The main part of the club cut in two for those with a low Z height printer
* headB - The companion to headA

* end1 - Half of the knob at the handle end of the club. Print this in rubbery TPU
* end2 - The other half of the knob
* end3 - Optional final touch to round off the end. Glued in place.
  The end is in parts, so that each can be printed without overhangs.

* endWasher - Optional. Only when using the endRing. Printed in PLA.

* handle - A TPU Tube. Optional - You could wrap the handle with cloth or
  tape instead.

* cap - A rounded piece to glue onto the head. Print in rubbery TPU.

* stop - In the middle of the club. Screws into the wooden dowel that runs the
  length of the club, but the screw is hidden from view. The other screw
  is in the top of the head, which must be inserted second, and then the
  cap glued on to hide the screw.

* spacer - Optional. Merely decorative.

* exploded - For documentation purposes only. Shows an exploded view of all the
  pieces. The head uses only one piece, so headA and headB aren't shown.

* inspect - For debugging only. A slice through pieces headA and headB to check
  the join.

## Print Notes

Change your slic3 settings so that cap, end, endB, endRing and spacer use
TPU material. This may have different temperature values to PLA.

Other slicer settings have been hard coded in "fun slic3rOverrides" at the
end of this file. If you slice this manually, look at the recommended slicer
settings there.

## Construction

Take piece "head" (or fit the "headA" and "headBb" together)
and insert the dowel and the "stop".

Mark where the stop fits on the dowel. 
Remove the head, and screw the stop to the dowel (Drill a pilot hole first)

If you printed the head in two parts headA/headB glue them together with expoy glue.
Replace the head, and screw together at the end of the clud (Drill a pilot hole first).

Push the handle into place, and measure and cut the rod to the final length.
Note that the rod also goes into "end1".

Place end1, end2 and endWasher, then screw in place (pre drill the dowel).
Optionally glue on end3 to round off the end and hide the screw.

Happy juggling. Practice your flairs while printing the next club ;-)
For my favourite single clubetrick, search YouTube for : kick up juggling club
Practice with both feet. You will have nailed it by the time the 2nd club is
printed ;-)


NOTE. If you glue the screws to the end and head pieces, and let it set, then
you can unscrew the club without breaking any glue seals. Maybe it will get
loose over time though.

FYI, you can use ordinary wood screws, but I used threaded inserts, and
machine screws, so that unscrewing and rescrewing doesn't loosen the joint.
(If that happens, a wooden match in the hole is usually enough to make it tight again)
