Inspired by [Maker's Muse ](https://www.youtube.com/watch?v=wb29-ULRBaE)

Solid which roll in weird ways!
The simplest if a bi-cone, (two cones with attached at their bases).
This bi-cone is then sliced down the middle, and the two pieces rotated by 90 degrees.

Note, this is NOT how the shapes are constructed, but is simple explaination of their
shape. They are constructed by building the 2D cross section of the sliced bi-cone,
and the revolving that cross section through 180 degrees to form half of the solid.

The basic cross section shape (square for sides=4, hexagon for sides=6 etc)
can be extended by adding extra bits to each vertex. See @Custom extend.

Unlike Maker's Muse, I have made the connecting pieces circluar (his were polygonal).
This allows me to twist the two halves into place, and must be carefully aligned.
You may find the pieces rotate too easily, in which case, damage the surface of the
ring. I used a pair of wire cutters to make lots of diagonal marks.
If you prefer the polygonal approach look for the comments in the groove and ring parts.

Note, you can glue the ring into one of the halves if you want.

## Notes
    
The ring is optional. You could permanently glue the two halves together.

The distribution of mass isn't the same as a perfectly solid object.
The "join" will have more mass, as there are solid layers there.
The ring and grooves will also affect the mass distribution further.
I'm not sure how these impact on the rolling, but I think it may cause it
to speed up and slow down, because in some directions of travel there will be
a larger angular momentum than in other directions when the object is travelling
at the same linear speed.

To understand this, just consider the ring, and imagine it is MUCH denser than
the main parts (whereas in reality, it is only slightly denser).
The angular momentum will be different when the ring is spinning about its axis,
compared to when it is spinning at 90 degrees to its axis (I think). Maths hard!
To test this, make the ring out of metal, preferably lead.
