See [https://en.wikipedia.org/wiki/Penrose_tiling]

Sir Roger Penrose gets miffed when people use his designs commercially without his consent.
He gets really miffed when people get the tiling wrong!

If you change filament during the printing process, you can create three colour pieces
(due to the traces being cut to different depths).

## Print Notes

Set Retract Lift to 0 to ensure that changeFilament works correctly.

Use TPU for the lids

## Colours
(In case I need reprints)

I used 3DQF's White, Sky Blue, and Navy Blue as well as Eryone's Galaxy Red (for the darts' top layer).
