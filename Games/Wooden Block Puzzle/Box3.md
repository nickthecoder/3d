A three part box. There are two "lids", and a tube joining them.

FYI, I have created another version of this in Boxes/PuzzleBox.foocad
where the middle section is optional, or you can add many middle sections.
