Generates a puzzle piece for the classic "Wooden" Block puzzle.
Take 6 pieces, and arrange them into a 3x3x3 cube.

See Generate.feather for a script which generates all the pieces.
Note, you do not need a full set of pieces. 6 is all you need for a single puzzle.

See Solver.kts for a script which solves the puzzles, so that you can
see if a combination of pieces has a solution and if so, how easy it is
(the more solutions there are, the easier it is).

For a hard puzzle try pieces : P E T   A I D
Note, the "D" piece requires support material the others do not.

