/**
 * Run using :
 * kotlinc -script Solver.kts
 *
 * alias kotlinc=/local/idea-IC-191.7479.19/plugins/Kotlin/kotlinc/bin/kotlinc
 *
 * FYI, I created this script before Feather existed (otherwise I may have used
 * feather instead of Kotlin, so that all code used the same language).
 */

import java.util.*

class Point(val x: Int, val y: Int, val z: Int)
    : Comparable<Point> {

    override fun compareTo(other: Point): Int {
        if (x != other.x) return x - other.x
        if (y != other.y) return y - other.y
        return z - other.z
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Point) return false
        return x == other.x && y == other.y && z == other.z
    }

    override fun hashCode() = Objects.hash(x, y, z)

    override fun toString() = "[$x,$y,$z]"

}

class Block(
        val name: Char,
        val cubes: List<Point>
) {

    constructor(name: Char, vararg cubes: Point) : this(name, cubes.toList())

    fun normalised(): Block {
        //println("Normalising $this")
        val sorted = cubes.sorted()
        //println("Sorted = $sorted")
        val first = sorted.firstOrNull() ?: return this
        val normCubes = sorted.map { Point(it.x - first.x, it.y - first.y, it.z - first.z) }

        //println("Normalised $normCubes")
        return Block(name, normCubes.toMutableList())
    }

    fun renamed(name: Char) = Block(name, cubes)

    fun rotateZ() = Block(name, cubes.map { Point(-it.y, it.x, it.z) })

    fun rotateY() = Block(name, cubes.map { Point(-it.z, it.y, it.x) })

    fun rotateX() = Block(name, cubes.map { Point(it.x, -it.z, it.y) })

    fun mirror(name: Char = this.name) = Block(name, cubes.map { Point(-it.x, it.y, it.z) }.toMutableList())


    /**
     * Returns all the combinations of rotations
     */
    fun rotations(): List<Block> {
        val result = mutableSetOf<Block>()

        fun add(b: Block, message: String) {

            val norm = b.normalised()
            if (!result.contains(norm)) {
                result.add(norm)
                //println("Adding $name : $message")
                add(norm.rotateX(), message + "X")
                add(norm.rotateY(), message + "Y")
                add(norm.rotateZ(), message + "Z")
            } else {
                //println("Ignoring duplicate $message")
            }
        }

        add(this, "")

        return result.toList()
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Block) name == other.name && cubes == other.cubes else false
    }

    override fun hashCode() = Objects.hash(name, cubes)

    override fun toString() = "Block $name : ${cubes.joinToString()}"

}


class WoodenBlocks(
        val width: Int,
        val depth: Int,
        val height: Int
) {

    val voxels = Array(width) { Array(depth) { Array(height) { ' ' } } }

    fun dumpVoxels() {
        for (x in 0 until width) {
            for (y in 0 until depth) {
                for (z in 0 until height) {
                    print(voxels[x][y][z])
                }
                println()
            }
            println()
        }
    }

    fun solve(blocks: List<Block>) {
        solve(blocks) { _, wb ->
            println("First solution (there may be more!) :")
            wb.dumpVoxels()
            true
        }
        println()
    }

    fun solve(vararg blocks: Block, onFound: (Int, WoodenBlocks) -> Boolean): Int {
        return solve(blocks.toList(), onFound)
    }

    fun solve(blocks: List<Block>, onFound: (Int, WoodenBlocks) -> Boolean): Int {

        var solutionCount = 0

        // return true if we should exit.
        fun solvePart(rotations: List<List<Block>>): Boolean {
            if (rotations.isEmpty()) {
                return onFound(solutionCount++, this)
            }
            // We try to put the last block into place, then recursively call solvePart without that last block
            // in the list of rotations.
            val r = rotations.last()

            // b is each of the orientations of the last block.
            for (b in r) {
                // Try it in all positions within the large cube.
                for (x in 0 until width) {
                    for (y in 0 until depth) {
                        for (z in 0 until height) {
                            if (fits(b, x, y, z)) {
                                place(b, x, y, z)
                                if (solvePart(rotations.subList(0, rotations.size - 1))) {
                                    remove(b, x, y, z)
                                    return true
                                }
                                remove(b, x, y, z)
                            }
                        }
                    }
                }
            }
            return false
        }

        // Convert the list of Blocks to a list of List of Blocks where sub-list contains the block in every possible
        // orientation. (and also normalised!)
        var rotations = blocks.map { it.rotations() }.sortedBy{ it.size }
        // The last piece should be a non-symetrical one (if there is one).
        // We create only ONE orientation of this piece, which prevents duplicating solutions.
        rotations = rotations.map{ if (it === rotations.last()) listOf(it.last()) else it }
        solvePart(rotations)
        return solutionCount
    }

    private fun fits(block: Block, dx: Int, dy: Int, dz: Int): Boolean {
        for (p in block.cubes) {
            val x = p.x + dx
            val y = p.y + dy
            val z = p.z + dz
            if (x >= 0 && x < width && y >= 0 && y < depth && z >= 0 && z < height) {
                if (voxels[x][y][z] != ' ') return false
            } else {
                return false
            }
        }
        return true
    }

    private fun place(block: Block, dx: Int, dy: Int, dz: Int) {
        for (p in block.cubes) {
            assert(voxels[p.x + dx][p.y + dy][p.z + dz] == ' ')
            voxels[p.x + dx][p.y + dy][p.z + dz] = block.name
        }
    }

    private fun remove(block: Block, dx: Int, dy: Int, dz: Int) {
        for (p in block.cubes) {
            voxels[p.x + dx][p.y + dy][p.z + dz] = ' '
        }
    }

    fun charAt(x: Int, y: Int, z: Int) = voxels[x][y][z]
}

// one,   two,   three, v,    a, a2, b, b2, l, o, w, h, y     p, q z, s, t, f, four, d

val colors = mapOf(

        '1' to "[0,0,0]",
        '2' to "[0.3, 0.3, 0.3]",
        '3' to "[0.6, 0.6, 0.6]",
        'v' to "[0.9, 0.9, 0.9]",

        'A' to "[0.9, 0, 0]", 'a' to "[0.7, 0 ,0]",
        'I' to "[0, 0.9, 0]", 'i' to "[0, 0.7 ,0]",
        'E' to "[0.9, 0.5, 0.9]",
        'O' to "[0.5, 0.9, 0.5]",
        'U' to "[0.9, 0.9, 0.5]",
        'Y' to "[0.5, 0.5, 0.9]",
        'S' to "[0.9, 0.5, 0.5]",

        'P' to "[0, 0.9, 0.9]",
        'R' to "[0, 0.7, 0.7]",
        'D' to "[0.9, 0, 0.9]",
        'N' to "[0.7, 0, 0.7]",
        'T' to "[0.9, 0.9, 0]",
        'L' to "[0.7, 0.7, 0]",

        '4' to "[0.5, 0.9, 0.9]",
        'd' to "[0.5, 0.2, 0.9]"

)

fun toScad(dx: Int, dy: Int, wb: WoodenBlocks): String {

    val buffer = StringBuffer()

    val map = mutableMapOf<Char, MutableList<Point>>()
    for (x in 0 until wb.width) {
        for (y in 0 until wb.depth) {
            for (z in 0 until wb.height) {
                val c = wb.charAt(x, y, z)
                var existing = map[c]
                if (existing == null) {
                    existing = mutableListOf<Point>()
                    map[c] = existing
                }
                existing.add(Point(x, y, z))
            }
        }
    }


    buffer.append("translate( [$dx,$dy,0]*6 ) {\n")
    for (entry in map.entries.sortedBy { it.key }) {
        if (entry.key != ' ') {
            val color = colors[entry.key] ?: "[1,1,1]"
            val list = entry.value

            buffer.append("  translate([0,0,0]) color($color) {\n")
            for (p in list) {
                buffer.append("    translate( $p ) cube([1,1,1]);\n")
            }
            buffer.append("  }\n")
        }
    }
    buffer.append("}\n")
    return buffer.toString()

}

// Three or fewer cube pieces

val one = Block('1', Point(0, 0, 0))
val two = Block('2', Point(0, 0, 0), Point(1, 0, 0))
val three = Block('3', Point(0, 0, 0), Point(1, 0, 0), Point(2, 0, 0))
val v = Block('v', Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0))

// Four cube pieces

val A = Block('A', Point(0, 0, 0), Point(-1, -1, 0), Point(-1, 0, 0), Point(0, 0, 1))
val I = A.mirror('I')
val E = Block('E', Point(0, 0, 0), Point(-1, 0, 0), Point(1, 0, 0), Point(1, -1, 0))
val U = Block('U', Point(0, 0, 0), Point(-1, 0, 0), Point(1, 0, 0), Point(0, 1, 0))
val S = Block('S', Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(0, 0, 1))
val O = Block('O', Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(1, 1, 0))
val Y = Block('Y', Point(0, 0, 0), Point(1, 0, 0), Point(1, 1, 0), Point(2, 1, 0))

// Duplicates
val A2 = A.renamed('a')
val I2 = I.renamed('i')

// Five cube pieces (common)

val P = Block('P', Point(0, 0, 0), Point(0, 0, 1), Point(0, 1, 0), Point(1, 1, 0), Point(2, 1, 0))
val R = P.mirror('R')
val D = Block('D', Point(0, 0, 0), Point(0, -1, 0), Point(1, -1, 0), Point(0, 0, 1), Point(-1, 0, 1))
val N = D.mirror('N')
val T = Block('T', Point(0, 0, 0), Point(-1, 0, 0), Point(0, -1, 0), Point(1, 0, 0), Point(1, 0, 1))
val J = Block('J', Point(0, 0, 0), Point(-1, 0, 0), Point(-1, -1, 0), Point(1, 0, 0), Point(1, 0, 1))
val K = Block('K', Point(0, 0, 0), Point(-1, 0, 0), Point(0, -1, 0), Point(1, 0, 0), Point(-1, 1, 0))
val W = Block('W', Point(0, 0, 0), Point(-1, 0, 0), Point(1, 0, 0), Point(0, -1, 0), Point(0, 0, 1))

// These I haven't printed...
val B = Block('B', Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(1, 1, 0), Point(2, 0, 0))
val H = Block('H', Point(0, 0, 0), Point(-1, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(0, -1, 0))
val C = Block('C', Point(0, 0, 0), Point(-1, 0, 0), Point(-1, 1, 0), Point(1, 0, 0), Point(1, 1, 0))
val Z = Block('Z', Point(0, 0, 0), Point(-1, 0, 0), Point(-1, 1, 0), Point(1, 0, 0), Point(1, -1, 0))
val G = J.mirror('G')
val M = Block('M', Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(1, 1, 0), Point(0, 0, 1))
val F = Block('F', Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(0, 0, 1), Point(0, 0, 2))
val L = T.mirror('L')
val Q = Block('Q', Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0), Point(1, 1, 0), Point( 0,0,1))


fun WoodenBlocks.solveScad(dx: Int, dy: Int, vararg blocks: Block): Int {
    return solve(* blocks) { count, wb ->
        println(toScad(dx + count, dy, wb))
        true
    }
}

fun WoodenBlocks.solveScadWithScore(dx: Int, dy: Int, vararg blocks: Block) {
    return solveScadWithScore(null, dx, dy, * blocks)
}

fun WoodenBlocks.solveScadWithScore(message: String?, dx: Int, dy: Int, vararg blocks: Block) {

    val pieces = (message ?: "") + " " + blocks.joinToString(separator = "") { it.name.toString() }
    println("translate( [ ${dx * 6 - 2}, ${dy * 6}, 0] ) scale( 0.2 ) text( \"$pieces\", halign=\"right\" );")

    val total = solve(* blocks) { count, wb ->
        if (count == 0) {
            println(toScad(dx + count, dy, wb))
        }
        false
    }
    var score = total
    blocks.map { 24 / it.rotations().count() }.forEach {
        score *= it
    }

    println("translate( [ ${dx * 6 + 4}, ${dy * 6}, 0] ) scale( 0.2 ) text( \"$total : $score\" );")
}


fun WoodenBlocks.scadAllSolutions(message: String?, dx: Int, dy: Int, vararg blocks: Block) {

    val pieces = (message ?: "") + " " + blocks.joinToString(separator = "") { it.name.toString() }
    println("translate( [ ${dx * 6 - 2}, ${dy * 6}, 0] ) scale( 0.2 ) text( \"$pieces\", halign=\"right\" );")

    val total = solve(* blocks) { count, wb ->
        println(toScad(dx + count, dy, wb))
        false
    }
    var score = total
    blocks.map { 24 / it.rotations().count() }.forEach {
        score *= it
    }

    println("translate( [ ${dx * 6 + 4}, ${dy * 6}, 0] ) scale( 0.2 ) text( \"$total : $score\" );")
}

fun WoodenBlocks.pieceScad(dx: Int, dy: Int, block: Block) {

    println("translate( [${dx}, ${dy - 0.5}, 0]*6 ) scale(0.2) text( \"${block.name}\" );")
    solve(block) { _, wb ->
        println(toScad(dx, dy, wb))
        true
    }
}

fun test1() {
    System.err.println("Solving test1")

    WoodenBlocks(3, 3, 3).apply {

        
        solveScadWithScore("(Steve's) ablpzt", 0, 0, A, I, E, P, D, T) // 2 Solutions. Steve's difficult puzzle
        solveScadWithScore("(Nick's) aalpzt", 0, 1, I, I2, E, P, D, T) // 2 Solutions, which are really identical (as two pieces are the same).
        solveScadWithScore(0, 2, A, A2, E, P, D, T) // 14 solutions.

        solveScadWithScore(4, 0, A, I, E, R, D, T) // 11 Solutions.
        solveScadWithScore(4, 1, A, A2, E, R, D, T) // 2 Solutions
        solveScadWithScore(4, 2, I, I2, E, R, D, T) // 6 Solutions

        solveScadWithScore(8, 0, A, I, E, P, N, T) // 6 Solutions
        solveScadWithScore(8, 1, A, A2, E, P, N, T) // 6 Solutions
        solveScadWithScore(8, 2, I, I2, E, P, N, T) // 2 Solutions

        solveScadWithScore(12, 0, A, I, E, P, D, L) // 3 Solutions
        solveScadWithScore(12, 1, A, A2, E, P, D, L) // 12 Solutions
        solveScadWithScore(12, 2, I, I2, E, P, D, L) // No Solutions!

        solveScadWithScore(18, 0, O, A, E, P, D, L) // 6
        solveScadWithScore(18, 1, O, I, E, P, D, L) // 6
        solveScadWithScore(18, 2, O, A, E, P, D, T) // 10
        solveScadWithScore(18, 3, O, I, E, P, D, T) // 5

        solveScadWithScore(22, 0, O, A, E, P, N, L) // 15
        solveScadWithScore(22, 1, O, I, E, P, N, L) // 6
        solveScadWithScore(22, 2, O, A, E, P, N, T) // 4
        solveScadWithScore(22, 3, O, I, E, P, N, T) // 13

        solveScadWithScore(0, 5, U, A, E, P, N, L) // 2
        solveScadWithScore(0, 6, U, I, E, P, N, L) // None
        solveScadWithScore(0, 7, U, A, E, P, N, T) // None
        solveScadWithScore(0, 8, U, I, E, P, N, T) // 4

        solveScadWithScore(4, 5, S, A, E, P, N, L) // None
        solveScadWithScore(4, 6, S, I, E, P, N, L) // 1
        solveScadWithScore(4, 7, S, A, E, P, N, T) // 2
        solveScadWithScore(4, 8, S, I, E, P, N, T) // 3

        solveScadWithScore(0, 10, A, O, Y, P, D, T) // None
        solveScadWithScore(0, 11, I, S, Y, P, D, T) // None
        solveScadWithScore(0, 12, A, S, Y, P, D, T) // 1

        solveScadWithScore(4, 10, A, O, Y, R, D, T) // None
        solveScadWithScore(4, 11, A, S, Y, R, D, T) // None
        solveScadWithScore(4, 12, I, S, Y, R, D, T) // None

        solveScadWithScore(8, 10, A, O, Y, P, N, T) // None
        solveScadWithScore(8, 11, A, S, Y, P, N, T) // None
        solveScadWithScore(8, 12, I, S, Y, P, N, T) // None

        solveScadWithScore(12, 10, A, O, Y, P, D, L) // None
        solveScadWithScore(12, 11, A, S, Y, P, D, L) // None
        solveScadWithScore(12, 12, I, S, Y, P, D, L) // None
    }
}


// I have :
// D J K N P R T W
// A E I I O U Y S

// grep '^[aeiiouysdjknprtw][aeiiouysdjknprtw][aeiiouysdjknprtw]$'  ~/server/documents/wordlist-threeOrMore.txt

// Pick a word from the top set, and one from the bottom set, and try solving it.
/*
ads aid air any ape are ask asp ate auk awe
day des die doe due duo dye
ear eat eon era eta
ins ion ire irk its
jay joe joy
key
nay
oak oar oat ode one ore our out owe
pay pea pie
roe rue rye
sad sap sat saw set sew sin sip sir sit ski sky sod son sop sot sow spa spy sty sun sup
tao tau tea tie tis toe toy
uke ups
was way woe
yak yap yaw yen yep yet yew yin yuk
 */

/*
and ant apt ark art awn
dan daw den dew din dip don dot dry
end
ink
jar jaw jet jew jot jut
ken kid kin kip kit
nap net new nip nit nod nor not now nut
opt own
pad pan par pat paw pen per pet pew pin pit pod pot pow pre pro pry pun put
rad ran rap rat raw ray red rep rid rip rod rot row run rut
tad tan tap tar taw ten tin tip ton top tor tow try two
urn
wad wan war wed wet win wit wok won wop wry wye
*/

fun test2() {
    System.err.println("Solving test1")

    WoodenBlocks(3, 3, 3).apply {

        solveScadWithScore(0, 4, T,O,Y,  U,R,N ) //

    }
}
/*

       

 */
fun noSolutions() {

    WoodenBlocks(3, 3, 3).apply {
        solveScadWithScore(0, 3, W,E,D, J,O,Y) //
        solveScadWithScore(0, 1, W,E,T,  R,U,N) //
        solveScadWithScore(0, 0, W,E,T,  K,I,D) //
        solveScadWithScore(0, 6, P,E,T, A,S,P) //
        solveScadWithScore(0, 20, O,A,T, U,R,N) //
        solveScadWithScore(0, 11, T,O,P, W,A,Y) //
        solveScadWithScore(0, 12, T,O,P, P,A,Y) //
        solveScadWithScore(0, 13, W,A,S, T,O,P) //
        solveScadWithScore(0, 14, N,I,P, O,U,T) //
        solveScadWithScore(0, 15, K,E,Y, W,A,D) //
        solveScadWithScore(0, 16, W,R,Y, A,S,P) //
        solveScadWithScore(0, 17, W,R,Y, D,U,O) //
        solveScadWithScore(0, 18, W,R,Y, J,O,E ) //


        solveScadWithScore(0, 1, D,A,Y, K,I,P) //
        solveScadWithScore(0, 3, S,A,D,  K,E,N) //
        solveScadWithScore(0, 4, O,W,N,  K,E,Y) //
        solveScadWithScore(0, 5, Y,U,K,  A,R,T) //
        solveScadWithScore(0, 7, W,A,S, T,E,N) //

        solveScadWithScore(0, 12, T,I,P,  A,N,Y) //
        solveScadWithScore(0, 15, O,N,E,  W,A,R) //
        solveScadWithScore(0, 9, T,R,Y, O,U,T) //
        solveScadWithScore(0, 18, T,O,Y, W,A,R) //
        solveScadWithScore(0, 13, A,D,S,  W,I,N) //
        solveScadWithScore(0, 14, D,I,E,  W,A,R) //
        solveScadWithScore(0, 0, S, K, I, W, A, R)
        solveScadWithScore(0, 14, R, A, Y, G, U, N)
        solveScadWithScore(0, 12, S, A, D, J, E, W)
        solveScadWithScore(0, 11, R, U, N, S, I, R)
        solveScadWithScore(0, 10, S, K, Y, P, A, D)
        solveScadWithScore(0, 9, P, A, D, S, K, I)
        solveScadWithScore(0, 5, N, A, P, D, U, E)
        solveScadWithScore(0, 4, G, I, N, D, U, E)

        solveScadWithScore(0, 0, W, E, T, D, A, Y)
        solveScadWithScore(0, 1, S, K, Y, R, A, P)
    }
}


fun easy() {

    WoodenBlocks(3, 3, 3).apply {
        solveScadWithScore(0, 0, R,Y,E, U,R,N) // 8 : 64
        
        solveScadWithScore(4, 0, O,P,T,  I,N,S) // 9 : 864
        solveScadWithScore(4, 1, P,E,T, A,U,K) // 9: 36
        solveScadWithScore(4, 2, O,P,T,  I,N,S ) // 9: 864

        solveScadWithScore(8, 0, T,R,Y, P,I,E) // 16 : 64
        solveScadWithScore(8, 1, P,E,T, S,T,Y) // 20: 120
        solveScadWithScore(8, 2, P,E,T, J,A,Y) // 28: 224
        
        solveScadWithScore(12, 0, P,R,O,  T,E,A) // 70 : 1120
    }
    

}

fun best() {
    System.err.println("Best Puzzles")
    WoodenBlocks(3, 3, 3).apply {

        // 1 solution
        solveScadWithScore(-6, 0, S,A,T,  R,E,D) // 1 : 12
        solveScadWithScore(-6, 1, S,T,Y,  P,A,D) // 1 : 24
        solveScadWithScore(-6, 2, S,K,Y,  W,A,R) // 1 : 24
        solveScadWithScore(-6, 3, S,P,Y,  J,A,R) // 1 : 24
        solveScadWithScore(-6, 4, W,A,S,  K,E,N) // 1 : 24

        // 2 solutions
        solveScadWithScore("(Steve's)", 0, 0, P,E,T,  A,I,D)  // 2 : 16
        solveScadWithScore("(Nick's) ", 0, 1, T,I,P,  D,I2,E) // 2: 16
        solveScadWithScore(0, 2, P,I,E,  T,I2,N) // 2 : 24
        solveScadWithScore(0, 3, P,E,N,  S,A,T)  // 2 : 24
        solveScadWithScore(0, 4, R,I,D,  U,T,E  )  // 2 : 24

        // 3 solutions
        solveScadWithScore(6, 0, T,I,N,  E,A,R) // 3
        solveScadWithScore(6, 1, P,E,T, S,I,N) // 3 sin pit
        solveScadWithScore(6, 2, K,E,Y, P,A,D) // 3 : 24


        // 4 solutions
        solveScadWithScore(12, 0, T,O,E,  N,A,P) // 4 
        solveScadWithScore(12, 1, N,U,T,  P,I,E) // 4
        solveScadWithScore(12, 2, R,U,N,  A,P,E) // 4 : 32
        solveScadWithScore(12, 3, P,I,E,  W,A,R) // 4 : 32

        // 5 solutions
        solveScadWithScore(18, 0, D,O,E,  P,I,T) // 5
        solveScadWithScore(18, 1, P,E,T, Y,A,K) // 5: 20

        // 6 solutions
        solveScadWithScore(24, 0, T,A,N,  P,I,E) // 6 : 48
        solveScadWithScore(24, 1, T,R,I,  D,I2,E) // 6 : 48
        solveScadWithScore(24, 2, O,N,E,  T,R,I) // 6  48
        solveScadWithScore(24, 3, O,N,E,  R,A,T) // 6 : 192
        solveScadWithScore(24, 4, T,I,P, O,A,R ) // 6: 192

    }
}


fun allSolutions() {
    System.err.println("Best Puzzles")
    WoodenBlocks(3, 3, 3).apply {

        scadAllSolutions("(Steve's)", 0, 0, P,E,T,  A,I,D)
    }
}

fun pieces() {
    WoodenBlocks(3, 3, 3).apply {
        // Prints individual pieces, as a key to the solutions.
        // one,   two,   three, v,    a, a2, b, b2, l, o, w, h,     p, q, z, s, t, f, four, d, x, g, n, j
        System.err.println("Adding Pieces Key")

        pieceScad(0, -1, A)
        pieceScad(1, -1, A2)
        pieceScad(2, -1, P)
        pieceScad(3, -1, D)
        pieceScad(4, -1, T)
        pieceScad(5, -1, Y)
        pieceScad(6, -1, E)
        pieceScad(7, -1, O)
        pieceScad(8, -1, U)
        pieceScad(9, -1, S)

        pieceScad(0, -3, I)
        pieceScad(1, -3, I2)
        pieceScad(2, -3, R)
        pieceScad(3, -3, N)
        pieceScad(4, -3, L)
        pieceScad(5, -3, K)
        pieceScad(6, -3, B)
        pieceScad(7, -3, H)
        pieceScad(8, -3, W)
        pieceScad(9, -3, C)
        pieceScad(10, -3, J)
        pieceScad(11, -3, G)
        pieceScad(12, -3, Z)
        pieceScad(13, -3, M)
        pieceScad(14, -3, F)


        pieceScad(20, -3, one)
        pieceScad(21, -3, two)
        pieceScad(22, -3, three)
        pieceScad(23, -3, v)
    }

    /*
    System.err.println( "Adding pieces into 3x3x3 cubes" )

    WoodenBlocks(3, 3, 3).apply {
        solveScad( 0, -5, a,  b,  l,    p, z, t)
        solveScad( 1, -5, a2, b2, y,    q, s, d )
        solveScad( 2, -5, o,  w, y,     x, u, two, v ) // two, v instead of a 5.
        solveScad( 3, -5, h, three, g, f, four, j )

        solveScad( 4, -5, n, k, e, c )
        
        solveScad( 0, -7, o,  w, y,     x, u, c )
    }
    */
    System.err.println("Done")
}


fun boxed() {
    
    System.err.println("Boxed")

    
    val fours = listOf( A,E,I,I2,O,U,Y,S )
    val fives = listOf( D,J,K,N,P,R,T,W )
    val used = mutableListOf<Block>()
    
    fun solveOneBox(depth : Int, post : (String) -> Unit ) {

        
        for ( a in 0 until fours.size ) {
            if (used.contains( fours[a] ) ) continue
                
            for ( b in a+1 until fours.size ) {
                if (used.contains( fours[b] ) ) continue
                    
                for ( d in 0 until fives.size ) {
                    if (used.contains( fives[d] ) ) continue

                   
                    for (e in d+1 until fives.size ) {
                        if (used.contains( fives[e] ) ) continue
                            
                            val pieces = listOf(fours[a], fours[b], fives[d], fives[e])

                            if (depth == 2 ) {
                                val box = WoodenBlocks(3,3,2)
                                box.solve( pieces ) { count, wb ->
                                    if (count == 0) {
                                        //box.dumpVoxels()
                                        post("[$depth] : ${fours[a].name}, ${fours[b].name}, ${fives[d].name}, ${fives[e].name}")
                                    }
                                    false
                                }

                            } else {
                                val box = WoodenBlocks(3,3,3)

                                for ( c in b+1 until fours.size ) {
                                if (used.contains( fours[c] ) ) continue
                                    
                                for (f in e+1 until fives.size ) {
                                    if (used.contains( fives[f] ) ) continue
                            
                                    val pieces = listOf(fours[a], fours[b], fours[c], fives[d], fives[e],fives[f])
                                    used.addAll( pieces )
                                    box.solve( pieces ) { count, wb ->
                                        if (count == 0) {
                                            post("[$depth] : ${fours[a].name}, ${fours[b].name}, ${fours[c].name}, ${fives[d].name}, ${fives[e].name}, ${fives[f].name}")
                                        }
                                        false
                                    }
                                    used.removeAll(pieces)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    solveOneBox( 0 ) { sol1 ->
        solveOneBox( 1 ) { sol2 ->
            solveOneBox( 2 ) { sol3 ->
                println( "***** YES *****" )
                println( sol1 )
                println( sol2 )
                println( sol3 )
            }
        }
    }
}


//test2()
//best()
//pieces()
//allSolutions()
boxed()

