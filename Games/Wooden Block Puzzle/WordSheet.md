A thin "card" with four lots of 6 letter puzzle piece combinations.
Choose a corner, find the puzzle pieces corresponding to the 6 letters,
and try to make a 3x3x3 cube.
