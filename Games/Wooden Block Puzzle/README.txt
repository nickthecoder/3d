
If I make a set of pieces, including 2 a's and 2 b's, then I can try each of the combinations, without making more pieces.
But to make it easy to choose a puzzle, label each of the pieces, and then on the box, list the pieces needed for each
puzzle, with the puzzles ordered by difficulty (i.e. number of solutions?).

To make it interesting, can I label each piece with a letter, such that the puzzle I want spell out an English word?

A valid puzzle is made up of 3 four-cube pieces and 3 five-cube pieces.
I've used vowels for all of the four-piece cubes, and consonants for the rest.

Note, when I first started this, I had a different naming convention
(using letters which looked vaguely like the piece). So the variable names
in the kotlin script uses the old convention, but the labels use the new.

Here's the mapping of old to new :
a -> A
l -> E
b -> I
o -> O
w -> U
h -> Y
y -> S // Yikes run out of vowels !

z -> D
p -> P
q -> R
t -> T
s -> N
f -> L
