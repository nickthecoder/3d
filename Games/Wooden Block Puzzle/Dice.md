The idea is to roll a dice to choose which puzzle to solve.
I didn't use this in the end, and instead printed the puzzle choices
as plastic sheets.

You can use these sheets to pick a random puzzle - throw them, and pick the
corner that is closest to you.

