class Extras {

    func Polygon( sides : int, length : double ) =
        Circle( length / 2 / Degrees.sin( 180/sides ) ).sides( sides )


    func Triangle( length : double ) =
        PolygonBuilder().apply {
            moveTo(length/2,0)
            lineTo(0, Degrees.sin(60)*length)
            lineTo(-length/2,0)
        }.build()

    func Triangle( width : double, height : double ) =
        PolygonBuilder().apply {
            moveTo(0,0)
            lineTo(width,0)
            lineTo(0,height)
        }.build()

    func Triangle( width : double, height : double, topX : double ) =
        PolygonBuilder().apply {
            moveTo(0,0)
            lineTo(width,0)
            lineTo(topX,height)
        }.build()

    func Isosceles( width : double, height : double ) =
        PolygonBuilder().apply {
            moveTo(-width/2, 0)
            lineTo(width/2, 0)
            lineTo(0, height)
        }.build()

    /**
        A hexagon where the size is specified by the distance between opposite sides.
        NOTE, if you wish to make a hexagon by specifying the distance between opposite corners,
        just create a Circle(radius).sides(6).

        Two of the sides are parallel with the X axis. Rotate by 30 degrees for sides parallel to the Y axis.
    */
    func Hexagon( width : double ) : Shape2d {
        val hex = Circle(width).sides(6).toPolygon()
        return hex.scale( width / hex.size.y )
    }

    func Octagon( width : double ) = Square(width).roundAllCorners(width * 0.2925,1).center()

    /**
     A shape bounded by an arc and the two radii from the ends of the arc.
     In common english, this is often referred to as a "segment"
     e.g. "a segment of orange". But in maths segment has another meaning (see below).
    */
    func Sector( radius : double, fromAngle : double, toAngle : double ) : Shape2d {
        val from = Vector2( radius, 0 ).rotate(Math.toRadians(-fromAngle))
        val to = Vector2( radius, 0 ).rotate(Math.toRadians(-toAngle))
        val large = Math.abs(toAngle-fromAngle) > 180
        // If this is a large "backwards" arc, then we need to use the center of the
        // alternate circle.
        // Without this "if" we will end up with a weird triangle unioned with a circle.
        val center = if ( large && from.cross(to) > 0 ) {
            to+from
        } else {
            Vector2.ZERO
        }
        return PolygonBuilder().apply {
            moveTo(center)
            lineTo(from)
            circularArcTo( to.x, to.y, radius, large, true )
        }.build()
    }

    /**
     A shape bounded by an arc and the chord from the ends of the arc.
     Note. This is different from the common English meaning of segment
     (as in "a segment of orange"). See Sector above.
    */
    func Segment( radius : double, length : double ) : Shape2d {
        val proportion = length/2 / radius
        return if (proportion >= 1) {
            Segment( length/2, -90, 90 )
        } else {
            val halfAngle = Degrees.asin( proportion )
            Segment( radius, -halfAngle, halfAngle )
        }
    }

    /**
     A shape bounded by an arc and the chord from the ends of the arc.
     Note. This is different from the common English meaning of segment
     (as in "a segment of orange"). See Sector above.
    */
    func Segment( radius : double, fromAngle : double, toAngle : double ) : Shape2d {
        val from = Vector2( radius, 0 ).rotate(Math.toRadians(-fromAngle))
        val to = Vector2( radius, 0 ).rotate(Math.toRadians(-toAngle))
        val large = Math.abs(toAngle-fromAngle) > 180

        return PolygonBuilder().apply {
            moveTo(from)
            circularArcTo( to.x, to.y, radius, large, true )
        }.build()
    }

    func RingD( outerDiameter : double, innerDiameter : double ) =
        Circle( outerDiameter/2 ) - Circle( innerDiameter/2 )

    func Ring( outerRadius : double, innerRadius : double ) =
        Circle( outerRadius ) - Circle( innerRadius )

    func CircleD( diameter : double ) = Circle( diameter / 2 )

    func CylinderD( height : double, diameter : double ) = Cylinder( height, diameter / 2 )

    /**
     A "normal" cone, with the tip [hiehgt] above the center of the circle
    */
    func Cone( height : double, radius : double ) =
            ExtrusionBuilder().apply {
                crossSection(Circle( radius ))
                forward(height)
                point()
            }.build()
   
    func ConeD( height : double, diameter : double ) = Cone( height, diameter/2 )
    
    /**
     A cone, with the point at an arbitrary point
    */
    func Cone( tip : Vector3, radius : double ) =
            ExtrusionBuilder().apply {
                crossSection(Circle( radius ))
                moveBy(tip)
                point()
            }.build()
       
    
    /**
     A truncated cone. r1 is the radius at z=0. r2 is the radius at z=height
    */
    func Cone( height : double, r1 : double, r2 : double ) : Shape3d {
        return if (r1 < r2) {
            Circle( r2 ).scale( r1/r2 ).extrude( height, r2 / r1 )
        } else {
            Circle( r1 ).extrude( height, r2 / r1 )
        }
    }

    func CircularArc( radius : double, fromAngle : double, toAngle : double ) : Path2d {
        val large = (toAngle - fromAngle) > 180
        val right = Vector2( radius, 0 )

        return PolygonBuilder().apply {
            moveTo( right.rotate( Math.toRadians( -fromAngle) ) )
            circularArcTo( right.rotate( Math.toRadians( -toAngle ) ), radius, large, true )
        }.buildPath()
    }

    func CircularArc( outerRadius : double, innerRadius : double, fromAngle : double, toAngle : double ) : Shape2d {
        val large = (toAngle - fromAngle) > 180
        val outer = Vector2( outerRadius, 0 )
        val inner = Vector2( innerRadius, 0 )

        return PolygonBuilder().apply {
            moveTo( outer.rotate( Math.toRadians( -fromAngle) ) )
            circularArcTo( outer.rotate( Math.toRadians( -toAngle ) ), outerRadius, large, true )
            lineTo( inner.rotate( Math.toRadians( -toAngle ) ) )
            circularArcTo( inner.rotate( Math.toRadians( -fromAngle )), innerRadius, large, false )
        }.build()
    }

    func RoundedCircularArc( outerRadius : double, innerRadius : double, fromAngle : double, toAngle : double ) =
        (
            CircularArc( outerRadius, innerRadius, fromAngle, toAngle ) +
            Circle( (outerRadius - innerRadius)/2 ).rightTo( outerRadius ).rotate( fromAngle )
                .rotate( toAngle - fromAngle ).also()
        ).toPolygon()



    /**
     * Like [thickness], but with rounded caps at the start and end of the path.
     */
    func thicknessRounded( path : Path2d, thickness: double) = (
        path.thickness(thickness) +
            Circle(thickness / 2).translate(path.points[0]) +
            Circle(thickness / 2).translate(path.points[path.points.size()-1])
        ).toPolygon()

    func outline( shape : Shape2d, thickness : double ) = outline( shape, thickness/2, thickness/2 )

    func outline( shape : Shape2d, outsideThickness : double, insideThickness : double ) : Shape2d {
        val solids = listOf<Shape2d>()
        val holes = listOf<Shape2d>()

        for (path in shape.paths) {
            solids.add( shape.offset( outsideThickness ) )
            holes.add( shape.offset( -insideThickness ) )
        }

        val solidsShape = if (solids.size() == 1) { solids[0] } else { Union2d( solids ) }
        val holesShape = if (holes.size() == 1) { holes[0] } else { Union2d( holes ) }

        return solidsShape - holesShape
    }

    // NOTE that we can cannot created outlines where the void in the middle
    // is smaller than the original.
    func outline( shape : Shape3d, outsideThickness : double ) =
        shape.minkowski( Cube( outsideThickness*2 ).center() ) - shape

    func outlineWith( shape : Shape3d, outliner : Shape3d ) =
        shape.minkowski( outliner ) - shape

}

/*
class Parallelogram( val size : Vector2, val offset : float ) : Lazy2d() {

    constructor( size : Vector2 ) : this( size, 0 )

    meth deltaX( value : float ) = Parallelogram( size, offset )

    meth angle( angle : double ) = Parallelogram( size, size.y * Math.cos( angle / 180 * Math.PI ) )

    override meth build() : Shape2d {
        return PolygonBuilder().apply {
            moveTo(0,0)
            lineBy( size.x, 0 )
            lineBy( offset, size.y )
            lineBy( -size.x, 0 )
        }.build()
    }
}


class Trapezium( val size : Vector2, val offset : float ) : Lazy2d() {

    constructor( size : Vector2 ) : this( size, 0 )

    meth deltaX( value : float ) = Parallelogram( size, offset )

    meth angle( angle : double ) = Parallelogram( size, size.y * Math.cos( angle / 180 * Math.PI ) )

    override meth build() : Shape2d {
        return PolygonBuilder().apply {
            moveTo(0,0)
            lineBy( size.x, 0 )
            lineBy( offset, size.y )
            lineBy( -size.x - offset*2, 0 )
        }.build()
    }
}
*/

class MyModel : Model {

    override fun build() : Shape3d {
        return Cube(10)
    }
}
