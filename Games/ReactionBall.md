Head over to [https://www.thingiverse.com/thing:1040716] and download the stl files
for the ball(s) you are interested in. Set "ballName" appropriately.

Then generate the gcode for each "pieceNumber" 0..7

Note, because the stl files above are so detailed, it takes quite a while to build.
