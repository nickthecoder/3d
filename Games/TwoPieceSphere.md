A two piece hollow sphere.
Print two hemispheres (both exactly the same), and glue them together.
I believe TPU should be possible (not tried it yet).

The overhang is solved by creating a flat spot in the interior, so that the
interior poles use bridging before the angle gets too steep.

You can change the position of this flat spot using the @Custom overhang.

There are optional holes in the overlapping region. These can be used to
add extra mechanical strenth by inserting dowels or threaded rod prior
to gluing. Drill out the holes, insert the dowels with glue,
then drill out the holes on the opposite side 0.5mm bigger. This way the dowels
won't be pushed out when you close the sphere.
Even without dowels, the holes may help, as glue will enter them.

You can override outerSphere() to make a custom sphere, e.g. with writing
or other patterns.
