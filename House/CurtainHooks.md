You can buy these quite easily, but I wanted some RIGHT AWAY,
and I could only find them on-line.

The "glide" has been oriented so that the weakness of layer lines are as small as possible
However, the hoop part is connected to the neck by very little plastic. And the hoop itself
looks really weak!
The mass produced parts fail quite often (which is why I *needed* to print more), and so far
my 3D printed version seems weirdly stronger (I haven't seen a failure yet).

Unlike the mass produced parts, I can slot my glides in and out easily without taking the
rail apart. (I needed to shave the mass produced parts to do the same Grr).

FYI, the mass produced parts always fail at the thin part of the neck.
My parts are weakest at the hoop.
