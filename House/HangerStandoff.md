Let's me hang clothes against a wall/cupboard to dry,
such that they don't touch the wall.

Can be used for picture rails (`forPictureRail=true`)
as well as doors (`forPictureTail=false`).

