/*
    Fixes a pole between two vertical surfaces.
    I firswt wanted this for a simple shoe rack at the bottom of my wardrobe.
*/

include <ntc/tools.scad>;
include <ntc/countersink.scad>;

module rodFixing( rodD, thickness, overlap, screw )
{
    difference() {
        cylinder( d = rodD + thickness * 2, h=thickness + overlap);
        translate([0,0,thickness]) {
            hull() {
                cylinder( d = rodD, h=thickness + overlap );
                translate([rodD,0,0]) cylinder( d = rodD, h=thickness*2 );
            }
        }
        ntc_arrange_mirror() translate([5,0,thickness+0.1]) mirror([0,0,1])ntc_countersink_hole( screw=screw, recess=1 );
    }
}

/*
    Like rodFixing, except the hole accepts a rounded square, rather than a circle.
    Useful for a square piece of wood that has been rounded using a router.
*/
module roundedSquareFixing( size, roundness, thickness, overlap, screw )
{
    difference() {
        cylinder( d = size + thickness * 2, h=thickness + overlap);
        translate([0,0,thickness]) {
            hull() {
                ntc_arrange_mirror([0,1,0]) translate([roundness-size/2,roundness-size/2,0]) cylinder( r = roundness, h=thickness + overlap );
                ntc_arrange_mirror([0,1,0]) translate([100,roundness-size/2,0]) cylinder( r = roundness, h=thickness + overlap );
            }
        }
        ntc_arrange_mirror() translate([5,0,thickness+0.1]) mirror([0,0,1])ntc_countersink_hole( screw=screw, recess=1 );
    }
}

roundedSquareFixing( 20, 5, 4, 6, screw=[5,10,8] );
//rodFixing( 20, 4, 6, screw=[5,10,8] );

