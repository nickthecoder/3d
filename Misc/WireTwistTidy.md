Insert a cable half way along its length, then twist to gather it together without
knotting. Pull the two ends to unwind.
