A cap for a lithium ion battery, with a slot for the wire.
Designed to be press-fitted into place.