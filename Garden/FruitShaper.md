Shapes fruit, such as cucumbers, courgettes, while growing.
Place the fruit inside the tube when it is small enough to fit in easily.
Add the ribs, to hold the two halves closed.
Wait.

If the ribs slip too easily along the tube, add paper to wedge them in place.

## Print Notes

Use a generous rim for the tube, as it is tall, with very little bed contact.

Print as many ribs are you see fit. Fruit can exert quite a lot of pressure while growing.
They can easily bend a thin plastic tube!
