Holds a cold frame's lid, either closed or open.

The main rod is a wooden dowel, glued into the "tip" piece,
and slides through the "clamp" piece.
The "bolt" piece holds the dowel in a single position.

There is also anoth version using pieces "internalClamp" and "internalBolt",
which allows the dowel to be inside the coldframe, with the "internalBolt"
passing through a smooth hole in the frame.