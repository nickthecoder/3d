I'm reinventing plant labels.
Most of my seedlings are the same year after year. So why do I clean off the lables, and
write the names again each year? Because finding the correctl label is a PITA.

But it could be easy if I keep them in alphabetical order, and have different sets
(such as a flower set, a salad/herb set a vegetable set etc).

## Print Notes

My first batch used PLA, but this tends to warp over time
(when in the sun).
I may try again with PETG.

Use different colours for each "key ring".

* Red    : Fruit and Veg (excluding leafy veg)
* Yellow : Flowers
* Green  : Leafy Veg + Herbs

I've used the same colours for my seed boxes ;-)

## Colours
* Leaf Veg and Herbs : 3DQF British Racing Green?
* Other Veg          : 3DQF Slignal Red
* FLowers            : Filamentive Yellow
* Cukes              : Eryone Galaxy Red
* Stakes and slots   : 3DQF 2 of : Jet Black? Pearl Black? Regal Black?
