When growing salad leaves, it is better to sow little and often.
So I want small containers that can be swapped around as the plants grow.

The pots are stackable for storage in the winter.
Also, the angled sides makes it easier to print, as there is a smaller first layer
(and therefore less likely to warp off the bed).

I've only printed the 1x1, 2x1 and 2x2 pots.
Printed with ASA (Winkle Ash Gray) using a 0.6mm nozzle with 0.4mm layers, and a 20mm brim.
