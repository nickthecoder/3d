Place at the bottom of a container, so that water can freely drain without the soil
plugging the hole, or being washed out of the hole.
