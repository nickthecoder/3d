
Parts
=====

Base : ASA
----------

Insert : ASA
------------

Low fill-density

Perch : ASA
-----------
Should be a tight fit. Adjust "interferanceFit" if need be.

Connector : ASA
---------------
Should be a tight fit. Adjust "interferanceFit" if need be.

Tube : PETG
-----------

Time will tell how long the PETG will last.
AFAIK, PETG isn't UV stable (but ASA isn't transparent) :-(

Hanger : Galvanised Garden Wire
------
Don't print this, bend a piece of galvanised garden wire instead!
Poke the ends through the holes near the top of the tube.
