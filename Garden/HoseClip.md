A hose clip, so that the pipe does get buckled where it turns into my 4 way manifold.
I could buy these clips (designed for 15mm copper pipe), but I only want 2, and it was
quicker to design and print, than take a trip to the plumbers.
I also get to choose the colour by printing them myself.

## Print Notes

TPU/PETG is prefereable to PLA, because PLA is too stiff, and it helps if the ring bends to
let the pipe fit inside. However, I used PLA, as my pipe is flexible enough to squeeze in.

Standard hose pipes are "half inch", but this is the INTERNAL diameter, the extenal can vary.
Most of my pipes use an innerD of 15.5mm, but my oldest pipe needs 17mm
