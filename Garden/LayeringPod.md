A tool for air-layering plants.

Cut part of the bark from a stem, and cover with this pod.
Fill with moss/coir/compost. Keep it moist, but not waterlogged.
Fingers crossed, after several weeks, roots will have formed, and can be
cut to make a new plant.
