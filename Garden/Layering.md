Layering is a technique which creates genetic clones of plants.
Take a branch, and bend it over so that it touches the soil. Now secure it in place using a brick, or
a tent peg. That's it! The plant may send out roots, and you can the branch from the parent plant.
To speed up the process, you could slice the outer layer of bark where it will touch the ground.
Note that the roots tend to begin just below a "left node" i.e. where a leaf produded from the branch.

This model lets you layer plants without having to bend the branch to the ground.
Place the gromet (printed with TPU, or just use a piece of foam) around the prepared branch.
Slide on the cylinder, and slide the "join". Fill with growing medium (soil, compost, moss, vermiculite).
Add a little water and pop on the lid.
