I modified some shop-bought solar powered LED garden lights, so that they also have
wired power, and brighter LEDs. So I need new ground spikes to accommodate the 12V
wires.

My first attempt printed them upside down, but this made them weak along the (short)
layer lines. So this version I'm printing it laying on its side.

Adjust lenth and spikeT for your soil type. Hard soil, bump up spikeT, and lower length.

## Print Notes

Given the overhangs, don't go too crazy with the layer heights (but my prototype
printed ugly, but functional with a 0.3 layer height with 0.4 nozzle).

Use a brim. The "spike" has very little contact with the bed.

I used 5 perimeters (I wanted it to be solid)
