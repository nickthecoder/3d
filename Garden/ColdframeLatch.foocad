import uk.co.nickthecoder.foocad.threaded.v2.*
import uk.co.nickthecoder.foocad.screws.v3.*
import uk.co.nickthecoder.foocad.smartextrusion.v1.*
import static uk.co.nickthecoder.foocad.smartextrusion.v1.SmartExtrusion.*

class ColdframeLatch : Model {

    @Custom
    var diameter = 12
    
    @Custom
    var thickness = 3

    @Custom
    var clearance = 0.75

    @Custom
    var countersink = Countersink( 3, 10 )

    @Custom
    var internalBoltLength = 40

    meth thread() = Thread( diameter )
        .headSize( 30, 6 )

    meth internalThread() = Thread( 7 )
        .headSize( 30, 6 )

    meth profile() = Square( diameter + thickness*2, diameter + thickness*4 )
        .center()
        .roundAllCorners(2, 1)

    @Piece
    meth tip() : Shape3d {

        val length = diameter + countersink.recessDiameter + 2

        val internal = Circle( diameter/2 )

        val main = profile().smartExtrude( length )
            .top( Chamfer( 2 ) )

        val screwHole = countersink
            .depth( main.size.y + 10 )
            .recess( main.size.y - thickness )
            .rotateX( 90 )
            .topTo(main.top - 3 )
            .frontTo( main.front - 0.01 )

        val hole = Cylinder( diameter - 1, diameter/2 ).bottomTo(-0.1)

        return main - hole - screwHole
    }

    @Piece( print="clampPrint" )
    meth clamp() : Shape3d {
        
        val main = profile().extrude( diameter + thickness*2 ) -
            Cylinder( diameter*2, diameter/2 + clearance )
                .bottomTo( - 1 )

        val screwHole = countersink
            .depth( main.size.y + 0.02 )
            .recess( main.size.y - thickness )
            .rotateX( 90 )
            .centerZTo( main.middle.z )
            .frontTo( main.front - 0.01 )


        val thread = thread().threadedHole( diameter/2 + thickness*2 )
            .rotateX(90)
            .frontTo( main.front - 1 )
            .centerZTo(main.middle.z)            

        return main - screwHole - thread
    }

    @Piece
    meth clampPrint() = clamp().rotateX(90).bottomTo(0)

    @Piece( print="internalClampPrint" )
    meth internalClamp() : Shape3d {

        val main = profile().extrude( diameter + thickness*2 + 8 ) -
            Cylinder( diameter*3, diameter/2 + clearance )
                .bottomTo( - 1 )

        val screwHole = countersink
            .depth( main.size.y + 0.02 )
            .recess( main.size.y - thickness-2 )
            .rotateX( -90 )
            .topTo( main.top - thickness )
            .backTo( main.back + 0.01 )

        val thread = internalThread().threadedHole( diameter/2 + thickness*2 )
            .rotateX(90)
            .frontTo( main.front - 1 )
            .bottomTo(main.bottom + thickness)            

        return main - thread - screwHole
    }

    @Piece
    meth internalClampPrint() = internalClamp().rotateX(90).bottomTo(0)

    @Piece
    meth bolt() : Shape3d {
        return thread().bolt( thickness*2.5 )
    }

    @Piece
    meth internalBolt() : Shape3d {
        val rod = internalThread().threadedRod( internalBoltLength + 1 )
            .rotateY(90)
            .bottomTo( -1 )
        val shavedRod = rod intersection rod.boundingCube().bottomTo(0)
        val handle = Square( 6, 30 )
            .roundAllCorners(3)
            .smartExtrude( shavedRod.size.z )
                .edges( Chamfer(1) )
            .centerY()
            .rightTo( shavedRod.right+1 )

        return shavedRod + handle
    }

    @Piece
    meth stop() : Shape3d {
        val main = Circle( diameter/2 + thickness )
            .smartExtrude( diameter/2 + thickness )
            .bottom( Chamfer( 2 ) )
        val hole = Cylinder( diameter, diameter/2 )
            .bottomTo( thickness )

        return main - hole
    }

    meth displayExternal() : Shape3d {
        val stick = Circle( diameter / 2 ).extrude( 100 )
            .previewOnly()

        val clamp = clamp().translateZ(40)

        val bolt = bolt()
            .rotateX(-90)
            .centerZTo(clamp.middle.z)
            .backTo( -30 )

        val tip = tip().bottomTo( stick.top )
        val stop = stop()

        return stick + tip + clamp + bolt + stop
    }

    meth displayInternal() : Shape3d {
        val stick = Circle( diameter / 2 ).extrude( 100 )
            .previewOnly()

        val clamp = internalClamp().translateZ(40)

        val bolt = internalBolt()
            .rotateZ(-90)
            .centerZTo(clamp.middle.z)
            .backTo( -30 )

        val tip = tip().bottomTo( stick.top )
        val stop = stop()

        return stick + tip + clamp + bolt + stop
    }

    override meth build() : Shape3d {
        return displayExternal() + displayInternal().translateX(60)
    }
}