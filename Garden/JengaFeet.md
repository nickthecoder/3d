I made a giant Jenga from rounded carcassing timber from Wickes.co.uk,
and was given a woodeen box, which is just about big enough to fit all of the pieces.
We start the tower on top of the box
(so we don't have to bend down so far, and the tower is higher),
These feet fit under the box, and can be adjusted to make the platform level
(or deliberately angled if required!)

See also Hardward/AdjustableFeet.foocad, which is slightly different
(and uses a later version of the Threaded plugin).
