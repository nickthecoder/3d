My spout has degraded in the sun, but the body is still looking good.
3D printer to save a little plastic ;-)

This version comes in two parts, which should be glued together.
See version 2, for a single part version
(which may be trickier to print on plate slinger printers due to height and small bed contact).

NOTE. I've changed this script since printing it. Check that the rose will fit correctly!
