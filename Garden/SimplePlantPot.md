A simple plant pot, with thin walls, and a thicker rim for strength.
Fully customisable.
You can optionally print "feet", and glue them to the bottom, to help water drainage.
There is also a matching "saucer" to catch the water. 

I'm fed up of having lots of slightly different size plant pots, that do not stack easily.
Shops keep changing suppliers, so I cannot buy the same style pots that I bought a decade ago!

With a 3D printer, this problem dissappears, because *I* am in control of the size, and not
at the mercy of shops.

## Sizes

### Default : 67x80 52 base
The "standard" small sized pots. Wilko sells thin "disposable" pots of this size.
Fits 5x3 inside a gravel tray, loosely, poking way over the top.

### medium : 90x95 70 base
The "standard" sized pots. Wilko sells thin "disposable" pots of this size.

### module : 72x50 52 base
Fits 5x3 inside a standard gravel tray with little slack.

### smallModule : 43x50 ? base
