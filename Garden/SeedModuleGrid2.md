This is very similar to SeedModuleGrid, but the patten of the ribbing is different.
I'm not sure which is better yet, so I'm keeping both scripts for now.

I want a grid, which fits over a "standard" half size gravel tray.
The grid will hole SimplePlantPots, made to the appropriate size.
    
The default values work well with Wilko's half size gravel trays, and the "module" sized
SimplePlantPot. Alas, SimplePlantPot was sized so that it fit into a "full sized" gravel tray
(3x5), and they only just fint into a half size tray (3x2). Therefore the inner "ring" part
is removed, leaving the item weaker than I would like. (Because the central rib doesn't
attach to the outside edge).

## Parameters

* cutAt
  Cuts the final model into two parts, for a tray wider than the printer.
  cutAt is in the range 0..gridAcross - 1
  Use pieces cutA and cutB instead of the default.

* outerSize
  If outerSize is set, then it will raise the grid off the ground, making a
  tray (without a bottom) which helps keep pots together, not falling over,
  and is easy to move the whole tray, rather than individual pots.
  Alas, these trays are not stackable (but do not need to be very tall).
  Do NOT use outerSize and cutAt together.
