This was first created, so that I could secure a squash plant along the top of
a wooden fence. But I guess it could be used for lots of things.

## Print Notes

I suggest lots of perimeters, because the strength is needed along the length
(and it will be a 100% filled object due to the small thickness,
so why not all perimeters?)

I printed these in PLA, and they are flexible enough.
Time will tell if they hold their shape well enough.
