A bird feeder for birds that sit upright on a perch while feeding.

## Notes

After many years, my first feeder broke - probably due to UV damage, followed by
a heavy "handed" bird (or squirrel).
It was printed with PLA, but next time, I'll try PETG for the tube, and maybe ASA for the
other parts.

I thought about only reprinting the broken tube, but decided to model from scratch.
So check out BirdFeeder2.foocad.
