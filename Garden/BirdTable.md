A plan for a wooden bird table.

The "floor" section is not attached to the columns, so the whole top
can be lifted up.
A plastic, easy clean sheet can go under that part (green on the preview).

The roof is screwed semi-permanently to the columns section.
I'll use roofing felt to protect from the rain    

## In Hindsight

The fact that the floor is a plastic sheet an the whole
house lifts up makes cleaning much easier than shop-bought designs.
However, it's still trickier than expected.
When you lift up the house, and seeds etc. can spill
onto the base, and if you don't clear them all out,
the house won't fit back on correctly.

My Mum didn't like pidgeons taking over.
She tied string half way up the columns, so that only
smaller birds could feed.

I created a simpler version (See BirdTable2),
which I made for my garden.

Matthew has the original bird table now.
