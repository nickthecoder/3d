A simple bird feeder.

Goals :

* Easy to disassemble for cleaning
* Prevent seeds getting stuck, and going mouldy.

Shop-bought feeders often fail on both of these. Grr.
