import static uk.co.nickthecoder.foocad.along.v3.Along.*
import uk.co.nickthecoder.foocad.woodworking.v1.*
import static uk.co.nickthecoder.foocad.woodworking.v1.Woodworking.*
import static uk.co.nickthecoder.foocad.layout.v1.Layout2d.*
import static uk.co.nickthecoder.foocad.layout.v1.Layout3d.*

class SaladPot2 : AbstractModel() {

    @Custom
    var height = 80
    
    @Custom
    var singleSize = 100

    @Custom
    var lipSize = Vector2( 3, 10 )

    @Custom
    var thickness = 1.5

    @Custom
    var baseThickness = 1.2

    @Custom
    var offset = 14
    
    @Custom
    var bottomRadius = 30

    @Custom
    var topRadius = 10

    @Custom
    var holeDiameter = 5

    @Custom
    var holesPerUnit = 3

    @Custom
    var ribSize = Vector2( 1.6, 2.4 )

    @Piece
    fun pot1x1() = pot(1,1)

    @Piece
    fun pot2x1() = pot(2,1)

    @Piece
    fun pot3x1() = pot(3,1)

    @Piece
    fun pot2x2() = pot(2,2)

    @Piece
    fun pot3x2() = pot(3,2)

    fun pot( x : int, y : int ) : Shape3d {
        val size = Vector3(
            singleSize * x,
            singleSize * y,
            height
        )
        val rect = Square( size.x, size.y ).center()
        val base = rect.offset(-offset).roundAllCorners( bottomRadius )
        val belowLip = rect.offset(-lipSize.x).roundAllCorners( topRadius )

        val pot = ExtrusionBuilder().apply {
            crossSection( base.offset(-1) )
            forward( baseThickness)
            crossSection(1)

            forward( size.z - baseThickness - lipSize.y )
            crossSection( belowLip )
            forward( lipSize.y  - lipSize.x )
            crossSection( lipSize.x )
            forward( lipSize.x )
            crossSection()

            // Top
            crossSection( -lipSize.x )
            // Now downwards
            forward( -lipSize.y )
            crossSection( belowLip.offset( - thickness ) )

            forward( -size.z + baseThickness + lipSize.y )
            crossSection( base.offset(-thickness) )

        }.build()

        val hole = Cylinder( 100, holeDiameter/2 )
        val spacing = (singleSize - offset/x)/holesPerUnit
        val holes = hole.repeatX( x * holesPerUnit - 1, spacing )
            .repeatY( y * holesPerUnit, spacing )
            .center() +
        hole.repeatX( x * holesPerUnit, spacing )
            .repeatY( y * holesPerUnit - 1, spacing )
            .center()
        
        val ribShape = Square( 2000, ribSize.x ).center().rotate(45)
        val ribsShape = ribShape
            .repeatX( (x + y - 1) * holesPerUnit, spacing )
            .center()
            .mirrorY().also()

        val ribs = (ribsShape intersection base).extrude( ribSize.y ).bottomTo( baseThickness )

        return pot - holes + ribs
    }

    @Piece
    fun stacking() : Shape3d {
        val x = 2
        val y = 1
        val a = pot( x,y )
        val b = pot( x,y ).color("Green").translateZ(12)
            
        return a + b 
    }

    // This will sit on a piece a toughened glass, so there's no need for a base.
    @Piece( printable = false )
    fun case() : Shape3d {
        val wood = Wood( "decking", 90, 25 )
        val frontBack = wood.cut( singleSize*8 + wood.thickness* 3 )
            .edgeUpAlongX()
            .translateY( singleSize * 4 + wood.thickness ).also()
            .centerXY()
            .color("LightGreen")

        val center = wood.cut( singleSize * 4 )
            .edgeUpAlongY()
            .centerXY()
            .color("Green")
        val sides = center.leftTo( frontBack.left )
            .rightTo( frontBack.right ).also()

        return frontBack + center + sides
    }

    @Piece( printable = false )
    override fun build() : Shape3d {
        val case = case()

        val a = pot( 2, 2 ).leftTo( case.left + 25 ).color("lightGrey")
        val b = pot( 1, 2 ).color("grey").leftTo(a.right)
        val c = pot( 1, 1 ).color("darkGrey")
            .leftTo(b.right)
            .frontTo(0).mirrorY().also()
        
        val pots = (a + b + c).frontTo(0).mirrorY().also()
        return case + pots.mirrorX().also()
    }

}