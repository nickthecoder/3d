A box for hibernating hedgehogs.
The box is made from planks of wood in two "stories".
Cut one plank in half lengthways (see halfDecking)

The roof is a cheap paving slab.

Build the two stories, and then connect them with a few pocket hole screws.
Then attach the floor from the bottom.
NOTE. The floor needs small gaps (4mm?) for expansion.

Finally add the light green "tilt" piece.
I'm not sure if this is needed. The idea is to lift the floor up slightly,
and ensure that water cannot pool in the sleeping quarters ;-)

Note, in the rendered version the floor extends beyond the entrance.
This last plank can be cut flush with the box.
The offcut can be used for the light green "tilt" piece ;-)

Each year, add a new corregated cardboard carpet ;-) and some straw.

For a professionally made version see :
[https://www.green-feathers.co.uk/collections/bird-boxes/products/eco-friendly-hedgehog-house]
