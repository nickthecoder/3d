I want to keep my seeds in a box, so that I can rummage through them easily.
The seeds are in paper envelopes about 15cm x 8cm, but some are wider than 8cm.

I highly recommend printing "testClasp" or "testHinge" before printing the
main box, to check that the hinge's "slack" and "gap" settings are suitable
for your printer.

## Colors

* Fruit and Veg        : Red
* Flowers              : Orange / Yellow
* Leafy Greens & Herbs : Green
