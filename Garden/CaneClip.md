I want some rings to support plants growing up canes.
The pieces are printed in TPU for a rubbery grip.

The large hoop's inner radius should be a little smaller than the cane's diameter.
The other hoop can be any size (depending on the size of the plant's stem).