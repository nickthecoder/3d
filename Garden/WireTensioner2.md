When running wires between posts, this gizzmo helps to tension the wires.
I believe they are called `turnbuckles`.

Do NOT use PLA, as it deforms when under a constant stress/strain.
I recomment ASA, or PETG.

The threads are mirror images, so turning the `adjument` piece will tighten/loosen the wire.
The nuts is optional, but can act as lock-nuts to prevent the thread loosening.

_Usage_

1. Optional. If using the lock-nuts, screw fully onto the bolts.
2. Screw both bolts slightly into the adjustment piece.
3. Fix both wires in place.
4. Turn the hex to tension the wire.
5. Optional. Tighten the lock nut(s).

