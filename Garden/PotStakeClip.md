If you need to stake a tall plant in a pot of compost, the compost is often too loose and fluffy
to support the stake. So I want to fasten a canehorizontally across the top of the pot, and fasten
another cane to it, vertically.

Use two "otherClip"s to fasten the horizontal cane to the pot
(otherD is the thickness of the pot's rim)
And use two "clip"s glued at right angles to fasten the vertical cane to the horizontal cane.

## Print Notes

Use a brim
