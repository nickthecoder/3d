I want a grid, which fits over a "standard" half size gravel tray.
The grid will hole SimplePlantPots, made to the appropriate size.
    
The default values work well with Wilko's half size gravel trays, and the "module" sized
SimplePlantPot. Alas, SimplePlantPot was sized so that it fit into a "full sized" gravel tray
(3x5), and they only just fint into a half size tray (3x2). Therefore the inner "ring" part
is removed, leaving the item weaker than I would like. (Because the central rib doesn't
attach to the outside edge).
