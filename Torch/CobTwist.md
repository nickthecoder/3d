A torch with a wide COD LED strip which can lay flat on flat surface
The light can be directed up/down by simply twisting the body.

Powered by a 18650 battery, through a boost circuit to raise to 12V needed
for the LED cob. Includes a USB battery charge port at the other end to the
on/off switch.

The feet can be taken on/off if required.

The feet include impressions for magnets to glued into.

The battery is held in place using a friction fit using tape to pad
it out slightly. MI used masking tape because it has high friction.
Ensure that you cover the terminals well. It could slide about!
I also taped the end of the charger module to be extra safe, and the
boost module is 100% protected by the "boostHolder" pieces.

Build parts
-----------

Build in several parts. Use the custom "piece" field :
    foot (x2)
    tube
    chargerEnd
    switchEnd
    boost (x2) (to secure the boost circuit within the tube) Required M3 bolts

Bought parts
------------

A 18650 battery. I use 2nd hand ones from old laptops. Usually only one or
two cells have died when the battery as a whole is dead. I don't tend to
use torches for long periods, so a high capacity battery is a waste!

Search ebay (or other shop) for :
    usb battery charge module
    dc boost module mounting holes (3.x V to 12V no usb socket

FYI, The boost modules with mouting holes are becoming rarer.
If you can't find any, the "boost" piece won't be of use.

Requirements
------------

    At least 17cm build height!

Possible Improvements
---------------------

    Add posts to the "buttonEnd", so that it can never rotate (twisting the wires)
    
    Cut away the tube to allow space for the wires from the LED cob strip.

