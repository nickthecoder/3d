A torch which uses a 9V battery, and a COB LED.
Customise the "ledBase" piece for particular LED, and also use it to
hold a buck or boost module to drive the COB LED at the correct voltage.
