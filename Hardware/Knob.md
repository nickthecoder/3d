A simple knob.

Use a threaded insert in the screw hole.

Beware that a wide and short knob would require support material to print in one piece.
