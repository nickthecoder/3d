Drill a hole in a piece of wood, and then insert this plastic trim
to neaten up the hole, and prevent water from getting in.

Wood glue will hold it in place, even though wood glue doesn't stick to plastic.
(due to the layer lines).

For a slot shaped hole (rather than a circle) :
    Mark two points `slotDistance` apart
    Drill the two holes
    Clean up the remainder with a chisel.