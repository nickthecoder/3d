import uk.co.nickthecoder.foocad.screws.v3.*

class SlatFixture : AbstractModel() {
    
    @Custom
    var slatSize = Vector2( 20, 30 )

    @Custom( about="A layer of plastic that the end grain touches. May be zero." )
    var baseThickness = 1.0

    @Custom
    var thickness = Vector2( 10, 16 )

    @Custom
    var chamfer = 3.0

    @Custom( about = "The diameter of the hole for the screw" )
    var screwD = 3

    @Custom( about = "The diameter of the countersink for the screws" )
    var headD = 8

    override fun build() : Shape3d {
        val outer = 
            Square( slatSize.x + thickness.x*2, slatSize.y + thickness.x ).centerX()
                .roundCorner( 1, chamfer ).roundCorner(0, chamfer)
        val profile = outer -
            Square( slatSize ).centerX().translateY( thickness.x + 0.01 )

        val base = outer.extrude( baseThickness )

        val main = profile.extrude( thickness.y )
        val hole = Countersink( screwD, headD).recess(10).topTo( 10 + main.top*0.5)
        val holes = hole
            .translate( slatSize.x/2 + thickness.x/2, thickness.y + slatSize.y*0.4, 0)
            .mirrorX().also() +
            hole.translateY( thickness.x / 2 )

        val angle = Degrees.atan( main.size.z*0.7 / main.size.y )
        val slope = Cube( main.size.x + 1, main.size.y * 2, main.size.z * 2)
            .centerX().rotateX(-angle).translateZ( main.top )


        return base + main - slope - holes
    }
}

