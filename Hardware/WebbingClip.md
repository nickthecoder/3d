Various parts for attaching webbing, found on ruck sacks etc.
All parts are customisable, for use with various thickness of webbing.

I started off creating the common quick-release clip, however, while checking out existing
solutions, I realised that the "Double D" is much strong, and simpler than the
quick-release clip.

So despite spending quick a long time perfecting the quick-release clip, I doubt I'll use it.
Double D is stronger and velco is quicker ;-)

Both halves of the quick-release clip are printed flat on the bed,
so that the forces do NOT act across the layer lines.
The prongs snapping in tension is the expected failure mode.

The male part of the quick-release clip must NOT use PLA (it is too stiff, and brittle).
I've been using ABS during testing, but PETG should work well too.

