Creates a piece with movable parts, with various clearances.
You can then use thi info when making your own model.
My cheap no-brand Chinesse printer with cheap filament can only handle slack
of 0.4 when printed using 0.2 layer height.

To insect a failure, cut through the opposite half from where you want to
inspect, and then snap the piece in half. Hopefully this will reveal more than
cutting would.

Interestingly Slic3r changes behavior between slack of 0.1 and 0.2.
The 0.2 i sliced as expected, but the 0.1 i sliced so that the pieces are fused!
This is with a 0.2 layer height - I should check if a smaller layer height
changes the point at which it decides to fused the pieces.
