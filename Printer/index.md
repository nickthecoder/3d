# Printer
## [AdjustableZStop](AdjustableZStop.foocad)

[![AdjustableZStop](AdjustableZStop.png)](AdjustableZStop.foocad)

## [AngleCalibration](AngleCalibration.foocad)

[![AngleCalibration](AngleCalibration.png)](AngleCalibration.foocad)

## [CalibrateX](CalibrateX.foocad)

[![CalibrateX](CalibrateX.png)](CalibrateX.foocad)

## [CalibrateY](CalibrateY.foocad)

[![CalibrateY](CalibrateY.png)](CalibrateY.foocad)

## [CalibrationCube](CalibrationCube.foocad)

[![CalibrationCube](CalibrationCube.png)](CalibrationCube.foocad)

## [Dave](Dave.foocad)

[![Dave](Dave.png)](Dave.foocad)

## [DustFilter](DustFilter.foocad)

[![DustFilter](DustFilter.png)](DustFilter.foocad)

## [Eddy](Eddy.foocad)

[![Eddy](Eddy.png)](Eddy.foocad)

## [FirstLayerTest](FirstLayerTest.foocad)

[![FirstLayerTest](FirstLayerTest.png)](FirstLayerTest.foocad)

## [Frank](Frank.foocad)

[![Frank](Frank.png)](Frank.foocad)

## [GeniusFilamentGuide](GeniusFilamentGuide.foocad)

[![GeniusFilamentGuide](GeniusFilamentGuide.png)](GeniusFilamentGuide.foocad)

## [GeniusSpoolRoller](GeniusSpoolRoller.foocad)

[![GeniusSpoolRoller](GeniusSpoolRoller.png)](GeniusSpoolRoller.foocad)

## [LevelCalibration](LevelCalibration.foocad)

[![LevelCalibration](LevelCalibration.png)](LevelCalibration.foocad)

## [NozzleChangeTool](NozzleChangeTool.foocad)

[![NozzleChangeTool](NozzleChangeTool.png)](NozzleChangeTool.foocad)

## [Organiser](Organiser.foocad)

[![Organiser](Organiser.png)](Organiser.foocad)

## [Pi3Case](Pi3Case.foocad)

[![Pi3Case](Pi3Case.png)](Pi3Case.foocad)

## [PinHeaderCover](PinHeaderCover.foocad)

[![PinHeaderCover](PinHeaderCover.png)](PinHeaderCover.foocad)

## [SlackCalibration](SlackCalibration.foocad)

[![SlackCalibration](SlackCalibration.png)](SlackCalibration.foocad)

## [SpoolPost](SpoolPost.foocad)

[![SpoolPost](SpoolPost.png)](SpoolPost.foocad)

## [SpoolRim](SpoolRim.foocad)

[![SpoolRim](SpoolRim.png)](SpoolRim.foocad)

## [SpoolSled](SpoolSled.foocad)

[![SpoolSled](SpoolSled.png)](SpoolSled.foocad)

## [X1QuickScrew](X1QuickScrew.foocad)

[![X1QuickScrew](X1QuickScrew.png)](X1QuickScrew.foocad)

## [ZLayerOffset](ZLayerOffset.foocad)

[![ZLayerOffset](ZLayerOffset.png)](ZLayerOffset.foocad)

