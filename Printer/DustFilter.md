Clip this to filament just before it enters the printer to prevent dust
being drawn into the hot end, where it may cause blockages, or mar the print.

I'm not 100% happy with the clip, the hinge works fine.

At some point I should probably make some reusable extensions for common
tasks such as hinges and clips! At which point, this should be reworked
to use them.

NOTE. On my Genius printer, this would fowl the extrusion gear, so you
should also print the GeniusFilamanetGuide.
