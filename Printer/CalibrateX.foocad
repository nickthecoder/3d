/*
Prints a bar which should be 100mm. Wait for it to cool.
I guess your printer should be recalibrate whenever you re-tensioning the belt, and
also periodically (as the belt will naturally loosen over time)???

If your printer needs adjusting, you can then use gcode  :

    M503     # View the current steps per unit
    M92 Xnnn # Set the new value for the steps per unit
    M500     # Save settings in EEPROM

Where nnn = currentSteps * 100 / actualLength

e.g. My test print was 97.9mm and M503 reported :
        echo:  M92 X78.74 Y78.74 Z2560.00 E105.00

    So
        78.74 * 100 / 97.9 = 80.43

    M92 X80.43

Print the model again, and repeated the proccess till it is as accurate as you want.
Then M500 to save the settings in EEPROM.

    M500

See https://marlinfw.org/docs/gcode/M092.html

If your printer doesn't support saving settings, then you could add the M92
command as preamble gcode within your slicer settings. Or if you are adventurous,
you could re-flash the firmware with corrected values (You did buy a printer with
free software right? ;-)

*/
class CalibrateX : AbstractModel() {
    
    var size = 100

    override fun build() : Shape3d {
        val cube = Cube(6).centerXY() - 
            Text( "X", BOLD, 4 ).center().extrude(5).translateZ(5.6)

        return cube.translateX(size-cube.size.x).also().centerXY() +
            Cube( size, 3, 0.8 ).centerXY() +
            Cube( size, 0.8, 3 ).centerXY()
    }
}
