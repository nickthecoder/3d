A filament spool holder, which runs on the internal hole of the spool.

Also requires a wooden dowel, a scrap of wood, two bearing.
Plus a shelf bracket (or use piece "wallBracket").

I found all-plastic versions on thingiverse, but they seemed very over engineered
(using a lot of plastic), and yet still not strong!

So my solution is multi-material. A wooden dowel to support the spool.
A metal shelf bracket with a thin "shelf" from a scrap of wood.
These are joined together with 2 pairs of plastic pieces with the bearings in between.

Wood is coloured yellow in the <No Piece> results.
The red and orange pieces are the only parts that are 3d printed.
Cut the dowel and wood scrap to suit your spool widths.

The red endGuides are simply glued to the wood scrap (the only take weight downwards,
so the strength of the joint isn't important)
The orange rodAxis can be glued to the dowel if you want, but will work without glue.

The piece "wallBracket" can be useful to attach the wooden "shelf" piece instead of
a metal shelf bracket. Not as strong as a metal shelf bracket, but has the advantage
that the wooden shelf can be taken out easily.

## Print Notes

For the wallBracket, use lots of perimeters / top & bottom laters / high infill.
6 perminters, 4 top & bottom solid layers 40% infill

