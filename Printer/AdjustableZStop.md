For my TwoTrees Bluer. Replace the simple z-stop with an adjustable one.
A twist of the knob will adjust the z offset by a tiny amount.

The square-ish "stop" piece screws into the upright 2040 extrusion, and
a long M4 screw is screwed tight to it, with the round knob pointing upwards
on the end of the screw.
Note the "stop" is printed on its side compared to its final orientation.
The thin part will point downwards.

Assemble the two pieces with a long M4 screw, with the knob screwed on half way.
Attach to the frame so that the knob hits the micro-switch when the hot-end is
roughly leveled at Z=0. This will give maximum adjustment.

When drilling out the holes, take care to keep them square.
