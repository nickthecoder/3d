import uk.co.nickthecoder.foocad.screws.v3.*
import static uk.co.nickthecoder.foocad.chamferedextrude.v1.ChamferedExtrude.*
import static uk.co.nickthecoder.foocad.layout.v1.Layout2d.*
import static uk.co.nickthecoder.foocad.layout.v1.Layout3d.*

/**
    A repair for a badly designed shower curtain.
    It was made in multiple pieces, so that it could be flat-packed, but the way the
    sections were joined was sub-optimal, and the 2 pieces of plastic broke.

    The pieces "main" and "top" are direct replacements (but stronger).
    However, consider using "topForRod" and "ceilingBracket" instead of "top",
    and join the rail to the ciling with a 12mm wooden dowel.

    NOTE "topForRod" doesn't include the extra pit which fits in the metal recess
    (to make it easy to print). I don't think the extra bit is important though.
*/

class ShowerCurtainConnector : AbstractModel() {
    
    var size = Vector3( 93, 11.2, 7.7 )
    var topSize = Vector3( 93, 15, 2.7 )
    var topExtra = Vector2( 5, 4.1 - 2.7 )
    var holeDist = 26
    var chamfer = 0.6
    var screwSize = Vector2(4,8)
    var rodD = 12

    @Piece
    fun main() : Shape3d {
        val main = Square(size.x, size.y).chamferedExtrude(size.z, 0, chamfer).centerXY()
        val holes = Cylinder( 20, 1.5 ).repeatX( 4, holeDist ).centerXY().translateZ(0.6)

        return main - holes
    }

    @Piece
    fun top() : Shape3d {
        var main = Square( topSize.x, topSize.y ).extrude( topSize.z ).centerXY()
        var extra = Cube( size.x, topExtra.x, topExtra.y ).centerXY()
            .bottomTo( main.top )
        val holes = Countersink( screwSize.x, screwSize.y ).repeatX( 4, holeDist ).centerXY().mirrorZ()

        return main + extra - holes
    }

    @Piece
    fun topForRod() : Shape3d {
        var main = Square( topSize.x, topSize.y ).extrude( topSize.z ).centerXY()
        val holes = Countersink( screwSize.x, screwSize.y ).repeatX( 4, holeDist ).centerXY()
            .topTo(main.top+0.1)

        val cylinder = (
            Square( holeDist - screwSize.y, main.size.y ).roundAllCorners(3).center() -
            Circle.hole( rodD/2+0.3 ).translateY(rodD/2-1) -
            Circle( screwSize.y/2).translateX( holeDist/2 ).mirrorX().also()
            )
            .extrude( 20 )
            .bottomTo( main.top )

        val otherHole = Cylinder( 30, 1.5 ).sides(4).rotateX(90).translateZ(18)
        return main - holes + cylinder - otherHole
    }

    @Piece
    fun ceilingBracket() : Shape3d {
        val main = ExtrusionBuilder().apply {
            crossSection( Circle( rodD/2 + 3 ) )
            forward(20)
            crossSection()
            crossSection( -3 )
            forward( -20+3 )
            crossSection()
        }.build()
        //val solid = Cylinder( 20, rodD/2 + 3 )
        //val hole = Cylinder( 20, rodD/2 ).translateZ(3) 
        val screw = Countersink( screwSize.x, screwSize.y )
        val sideHole = Cylinder( 30, 1.5 ).sides(4).rotateX(90).translateZ(20/2)
        return main - screw.topTo(3) - sideHole
    }

    override fun build() : Shape3d {
        return main() + top().translateZ(30)
    }
}

