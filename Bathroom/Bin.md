A bin for my bathroom. I have a shop-bought one, but it started to rust (cheap crap).

I considered a hinged lid, but I think a simple lid is better.
I also considered a pedal mechanism, but my original bin's mechanism broke,
and I doubt an all-plastic mechanism would be long lasting.

The liner is optional. It was included for 2 reasons :
	1. Allow the inside to be a different color from the outside (without the need for multi-filament prints).
	2. Allow the liner to be cleaned separately from the outside.
       This is only an issue if the bin has a hinged lid (which makes it harder to clean).

The knob is attached to the lid with a machine screw and a threaded insert.
