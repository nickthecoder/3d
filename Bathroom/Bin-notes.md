Print the knob with small layer height, otherwise it will show large steps
due to the small changes in Z on the front of the knob.

I used white PETG for the `main` and `knob` pieces.
PrimaSelect Metalic Blue PLA for the lid.
