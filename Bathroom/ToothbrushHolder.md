A wall-mounted toothbrush holder for 4 toothbrushes and toothpaste.
Each of the 5 holders are detachable for easy cleaning.

Similar shop-bought items do not have the long tubes, which is a problem.
Either the holes are too small for the brush handle,
or the toobrush head touches the holder (yuk!).

Each tube has a small hole at the bottom for drainage.

## Print Notes

Due to the height of the tubes, a large brim is recommended.

Use the customiser to print shorter tubes for child toothbrushes.

## Improvements

Make the toothpaste insert slightly wider.
