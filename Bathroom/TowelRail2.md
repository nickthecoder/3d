A towel rail which screws to a wall.
The rail itself is wood.

See TowelRail.foocad for a version which hooks over a shower screen.

## Usage

Use rounded headed screws.
