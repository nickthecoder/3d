A bin for toilet rolls.

The final size includes a `margin`, so `rollSize` can be exact.

This matches `Bin.foocad`.
