A towel rail to hang over a shower screen.
The rail iteself is wood.

Note, if you have a smaller diameter pole, then consider reworking the
profile in the svg document.

The "stickers" hide the screw that holds the plastic pieces to the wood.
Consider using epoxy glue as well as the screw.

## Print Notes

I used 20% infill. 2 perimeters, 4 bottom layers, and 4 top layers.
Increase the perimeters and infill for extra strength?

Use a different colour for the "stickers"?

## Possible Enhancements

1. In hindsight, I should have supported a countersunk screw, by making the
   recess shorted, and then the "stickers" could be quite thin.

2. The "stickers" could have pressed into a circular groove in the main part.
   This would preclude the use of glue.

3. Include little rubber (PTU) feet where the part touches the shower screen.
