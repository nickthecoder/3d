A door stop which is screwed to the floor.
Made in parts, with TPU for the rubbery rings.
Add a threaded rod (see rodHoleD) through the middle to screw it all together.

Assembly: Screw the bottom piece to the floor.
Screw the thread rod onto the bottom piece.
Stack the rings, rubber pieces and the middle section if the correct order.
Finally screw on the top, which will sandwich all the parts together.

## Print Notes

As it might get whacked by the door, don't be cheap on the infill nor perimeters.
I used 4 perimeters, and 40% infill.
For the tpu parts, I reduced this to 3 perimeters and 20% infill.
