The rose contains a power brick down to 5V.
Pointing outwards are multiple "super bright" LEDs glued/soldered to a metal disk.
These are powered via a normally open relay (maybe 2 or 3 circuits?), so they all turn on at
the beginning.

Control via remote control, and/or via wifi

If the arms are metal, then they can be bent upwards.

So that we don't need to route the data wire back down each arm, use a multiplexer,
and write to each arm in turn.

Print the cover using transparent PETG? With consentric circles?
