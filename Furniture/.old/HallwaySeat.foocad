import static uk.co.nickthecoder.foocad.along.v2.Along.*

class HallwaySeat : Model {

    @Custom var isExploded = false

    var unitWidth = 1000
    var unitDepth = 300
    var unitHeight = 1200

    var seatDepth = 400
    var feetHeight = 320

    override fun build() : Shape3d {
        val exploded = if (isExploded) 100 else 0

        val depth =  unitDepth + seatDepth
        val width = unitWidth

        val sheet = SheetLumber( "sheet", 12, "Orange" )
        val feetLumber = Lumber( "feet", 30, 50, "ForestGreen" )
        val trimLumber = Lumber( "trim", 80, 12, "LightGreen" )
        val supportLumber = Lumber( "support", 12, 25 )

        val mockUnit = Cube( width, unitDepth, unitHeight )
            .centerX().centerY().translate( 0, seatDepth/2, 0 )
            

        val floor = sheet.cut( width - trimLumber.thickness() * 2, depth - trimLumber.thickness() * 2 )
            .label( "floor" )
            .alongY2()
            .centerX().centerY()
            .translate( 0,0, -sheet.thickness() )

        val feet = feetLumber.cut( feetHeight )
            .label( "feet" )
            .translate( -width/2 + trimLumber.thickness(), -depth/2 + trimLumber.thickness(), -feetHeight - sheet.thickness() )
            .mirrorX().also().mirrorY().also()

        val edgeTrim = trimLumber.cut( depth - trimLumber.thickness() * 2 )
            .darker()
            .label( "edgeTrim" )
            .alongY().rotate(0,90,0)
            .centerY()
            .translate( -width/2, 0, 0 )
            .mirrorX().also()

        val endTrim = trimLumber.cut( width )
            .label( "endTrim" )
            .alongX()
            .centerX()
            .translate( 0, -depth/2, -trimLumber.width() )
            .mirrorY().also()

        val edgeSupport = supportLumber.cut( depth - trimLumber.thickness() * 2 - feetLumber.thickness() * 2 - 20 )
            .label( "edgeSupport" )
            .alongY()
            .centerY()
            .translate( -width/2 + trimLumber.thickness(), 0, -supportLumber.thickness() -sheet.thickness() )
            .mirrorX().also()

        val endSupport = supportLumber.cut( width - trimLumber.thickness() * 2 - feetLumber.width() * 2 - 20 )
            .label( "endSupport" )
            .alongX()
            .rotate( 90, 0, 0 )
            .centerX()
            .translate( 0, depth / 2 - supportLumber.width(), -supportLumber.thickness() -sheet.thickness() )
            .mirrorY().also()

        return mockUnit + feet + edgeTrim + endTrim + floor + edgeSupport + endSupport
    }

}

