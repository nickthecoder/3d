This is a demonstration of the "Construction" module, as well as the more general
components in FooCAD.
It is a design for a bed that I built (I didn't build the headboard)

Each part of the design starts off by defining the lumber (including the width and thickness),
then we cut the lumber to the required size, and optionally cut joints (such as a Lap joint,
Mitre joint etc).
Next we give the part a label (using Shape3d.label(name : String). All labelled parts
are arranged into a "plan", which shows each part type, and how many are needed.
This is a useful guide when cutting the parts.
Labelled parts are also collected together by the "BOM" (bill of materials), which
is handy when buying the wood.

We then position the parts, often duplicating them (as the design is symetrical).

Finally we combine all the parts together.

NOTE, FooCAD has improved since I created this model, so this script doesn't make use
of the newer features. (But I still use this as an example of the Construction module).
