import static uk.co.nickthecoder.foocad.layout.v1.Layout2d.*
import static uk.co.nickthecoder.foocad.layout.v1.Layout3d.*

class Backroom : Model {
    
    val alcove1 = Vector2(820,360)
    val alcove2 = Vector2(860,360)

    val room = Vector3(820+860+1420, 3660, 2580)

    val baseThickness = 30
    val base = Vector3(600, 2500, 737)

    val alcoveBaseDepth = 500
    val alcoveChimneyOverlap = 100

    val shelfDepth = 300
    val shelfShorterBy = 100

    // The height of the bottom of the first shelf
    val shelf1Height = 517 + 737
    // The distance from the bottom of one shelf to the bottom of the next.
    // Enough for a bottle of Galliano.
    val shelfDelta = 390
    // 
    val shelfThickness = 15
    
    @Piece( printable=false )
    override fun build() : Shape3d {
        val wallE = Cube( 100, room.y, room.z )
            .backTo(0)

        val wallN = Cube( room.x, 100, room.z ).frontTo(0).rightTo(0)
        val chimney = Cube( room.x - alcove1.x - alcove2.x, alcove1.y, room.z )
            .backTo(0).rightTo( -alcove2.x )

        val walls = (wallE + wallN + chimney).color( "MediumSlateBlue" )
        val floor = Cube( room.x, room.y, 10 ).topTo(0).backTo(0).rightTo(0)
            .color( "LightGrey" )

        val baseP = Square(base.x, base.y-alcoveBaseDepth)
            .backTo(-alcoveBaseDepth-10)
            .roundCorner(0, 200)


        val surface = baseP.extrude(baseThickness).rightTo(0)

        val alcove1SurfaceP = Square( alcove1.x + alcoveChimneyOverlap, alcoveBaseDepth )
            //.roundCorner(1, 70)
        val alcove1Surface = alcove1SurfaceP.extrude( baseThickness )
            .backTo(0)
            .leftTo(-room.x)

        val alcove2SurfaceP = Square( alcove2.x + alcoveChimneyOverlap, alcoveBaseDepth)
            //.roundCorner(0,70)
        val alcove2Surface = alcove2SurfaceP.extrude( baseThickness )
            .backTo(0)
            .rightTo(0)
            
        val surfaces = (surface + alcove1Surface + alcove2Surface)
            .bottomTo( base.z )
            .color( "Chocolate" )

        val end = Cube( base.x - 20, 15, base.z )
            .rightTo(0)
            .frontTo( -base.y + 300 )
            .color("GhostWhite")
        val post = Cylinder( base.z, 30 )
            .leftTo( -base.x + 30 )
            .frontTo( - alcoveBaseDepth - 40 )
            .tileY(2, -1000 )
            .color( "Grey" )

        val units = surfaces + end + post

        val shelf = Cube(shelfDepth, base.y - shelfShorterBy - shelfDepth, shelfThickness )
            .rightTo(0)
            .backTo(-shelfDepth-10)
        val alcove2Shelf = Cube( alcove2.x, shelfDepth, shelfThickness )
            .rightTo(0)
            .backTo(0)
        val alcove1Shelf = Cube( alcove1.x, shelfDepth, shelfThickness )
            .leftTo(-room.x)
            .backTo(0)

        val singleLevel = (shelf + alcove1Shelf + alcove2Shelf).bottomTo(shelf1Height)
        val shelfEnd = Cube( shelfDepth, 15, shelfDelta * 2 + shelfThickness)
            .rightTo(0)
            .bottomTo( singleLevel.bottom)
            .backTo( singleLevel.front )


        val shelves = (singleLevel.tileZ(3, shelfDelta-shelfThickness) + shelfEnd )
            .color("Chocolate")

        val door = Cube( 770, 40, 2000 )
            .rightTo(0).rotateZ(-70)
            .translate(-60, -room.y, 0)

        // Make sure the shelves are tall enough for a bottle of Galliano (to scale!)
        val galliano = PolygonBuilder().apply {
            moveTo(0,0)
            lineTo(0,350)
            lineTo(16,350)
            lineTo(18, 260)
            lineTo(35, 5)
            lineTo(32, 0)
        }.build().revolve().rightTo(-100).bottomTo( shelf1Height + shelfThickness ).frontTo(-1000)

        //println( "Rough floor area ${room.x /1000 * room.y / 1000} m^2" )
        //println( "Length of Surface ${surface.size.y + alcove1Surface.size.x + alcove2Surface.size.x}" )
        //println( "Shelves ${shelf.size.y} ${alcove1Shelf.size.x} ${alcove2Shelf.size.x}" )

        return (walls + floor + units + shelves + door + galliano)
            // I rotate it, just so that the auto-generated image & thumbnail look better.
            //.rotateZ(90)
    }
}

