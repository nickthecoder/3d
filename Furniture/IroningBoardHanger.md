I used to lean my ironing board against a wall behind a door. But if the angle is too shallow,
the board opens up (or falls over). If too steep, then the door hits it when the door opens.

My solution is to hand the board on the wall (so there is NO angle).
The metalwork of my ironing board has T shape metal tubes at the end of each of the two legs.

## Print Notes

I needed a brim (the first print lifted very badly).

Match the colour with the ironing board's cover ;-)
