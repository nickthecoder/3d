I have lots of shelving brackets, for 3 shelves, and rather than trying to align the brackets
perfectly on the wall, I add spacers of the required thickness to each bracket.

For thick spacers, use high perimeters, but low infill, as only the edge needs to take weight
(the bracket has a void in the middle).
