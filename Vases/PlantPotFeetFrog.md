Plant pot to help drainage, lifting the bottom of the pot off the ground.

The frog wasn't designed by me. See :      
    https://www.thingiverse.com/thing:18479
    by Morena Protti (https://www.thingiverse.com/morenap/designs)

NOTE, the original frog had many verticies, and OpenSCAD seemed to fail
(or maybe there was something weird about the original STL)

I lowered the number of polygons using MeshLab :
    Filters > Remeshing, simplification and construction > Quadratic Edge Collapse Decimation
Use the link above if you want the hi-res frog model.

Use the Scale ModelExtension to suit your pot.
Three frogs needed for each pot.
