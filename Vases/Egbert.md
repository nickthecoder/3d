Fill Egbert's head with compost, and grow some plants.

Note, the current design leaves a hole between the body and the head due to
the overhang being too big, but this doesn't affect the final result so I haven't
fixed it. But if this bothers you, add a flat section, (a cylinder)
so that it can use bridging before the overhang gets too big. Ahh, for the sitting
poses, you'd need to hollow out the body too.

