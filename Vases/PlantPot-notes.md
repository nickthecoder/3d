Print Notes

    For the largest pots, I used 0.3mm layer height, and "fast" settings.
    This may have made the pots weaker though. There were noticable faults
    in the layer lines. If using fsat settings again, try using higher temperatures.

I've coded information into my custom names, which are also included as text
on the bottom of the pot :

Sizes
    SHOT = Shot glass (39x42)
    XS = Xtra Small (62x55) - Tiny pots (I used them for small cactii)
    S = Small (80x75) - Smaller than normal
    M = Medium (95x90) - "Normal" size pots
    L = Large (150x140) - First printed for Amanda's birthday present
    LL = Larger (150x140) - First printed for Amanda's birthday present
    XL = Extra Large (250x233) - First printed for the big Raindow pot.

    H = Huge (150 x 140) - Old name, but same as "L"

    SQ = Small Square (70x81) - From Wilko
    MQ = Medium Square (93x100) - From Wilko

Modifiers of the basic shape :
    En Edges e.g. n=4 for square pots. Also for round pots in rounded polygons.
    Rn Resolution
    In Decorative Indents
    Vn RevolveResolution - 6ish for a vertical panelled effect.
