Creates a striped plant pot, by pausing the printer at various heights, allowing
you to change the colour of filament.

When designing, use piece "preview", so that you can see the colours in OpenSCAD.
When you are ready to print, set the piece back to  <No Piece>.
The stl file will then be built from the "regular" pot, rather than one made from many slices
(which would take ages).
Also note that when piece=preview, the slices have gaps between them, because otherwise
OpenSCAD renders all the slices in a single colour.

