
So many shop-bought decorative plant pots don't fit the non-decorative pots,
even from the same shop! Grrr. But now we can use 3D printers - Yay!

The "default" size 80x75 fits a good quality pot found in Wilko.
I print this with 0.2 layer height, 2 shells and 4 solid layers, 10% infill.

