Print Notes

* Zero infill
* Small layer height (because of overhangs).
* My "Family of three" were scaled as follows :
    pose1    : 100%
    sitting1 : 66%
    sitting2 : 80%

Post Print

Fill the body and legs with sand to make it stable.
I used thistle plaster instead of sand, as I had a nearly full sack spare.

